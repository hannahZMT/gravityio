//npm install socket.io redis

const config = require('./config.json');
//const redisconfig = config["redis"];
const redisconfig = config["redis"];
const authen = config["authenNode"];
const channels = config["channels"];

var http = require('http').Server();
var io = require('socket.io')(http);

var redis = require("redis");

io.on('connection', function (socket) {
    console.log(socket.id + ' connected');
    socket.on(authen.authenChannel.realTimeBets, (data) => {
        if (data["id"] == authen.inf["id"] &&
            data["key"] == authen.inf["key"]) { //authen
            console.log(socket.id + ' authen');
            var subscriber = redis.createClient({ port: redisconfig["port"], host: redisconfig["host"], auth_pass: redisconfig["auth_pass"] });

            subscriber.subscribe(channels.test);
            subscriber.subscribe(channels.realTimeBets.newBet);
            subscriber.subscribe(channels.realTimeBets.updatePT);
            subscriber.subscribe(channels.realTimeBets.newViolation);
            subscriber.on("message", function (channel, message) {
                console.log(socket.id + ' => Realtime-Bets:');
                console.log('        ' + channel + " : " + message);
                if (channel == channels.test)
                    io.to(socket.id).emit(channels.test, message);
                if (channel == channels.realTimeBets.newBet)
                    io.to(socket.id).emit(channels.realTimeBets.newBet, message);
                if (channel == channels.realTimeBets.updatePT)
                    io.to(socket.id).emit(channels.realTimeBets.updatePT, message);
                if (channel == channels.realTimeBets.newViolation)
                    io.to(socket.id).emit(channels.realTimeBets.newViolation, message);
            });

            subscriber.on('error', function (err) {
                console.log('Redis error: ' + err);
            });

            socket.on('disconnect', function () {
                console.log(socket.id + ' UnSub ');
                //subscriber.unsubscribe();
                //subscriber.end();
                subscriber.quit();
            });
        }
        else
            console.log(socket.id + " authen fail");
    })

    socket.on(authen.authenChannel.agentAccounts, (data) => {
        if (data["id"] == authen.inf["id"] &&
            data["key"] == authen.inf["key"]) { //authen
            console.log(socket.id + ' authen');
            var subscriber = redis.createClient({ port: redisconfig["port"], host: redisconfig["host"], auth_pass: redisconfig["auth_pass"] });

            subscriber.subscribe(channels.agentAccounts);
            subscriber.on("message", function (channel, message) {
                console.log(socket.id + ' => AgentAccounts:');
                console.log('        ' + channel + " : " + message);
                if (channel == channels.agentAccounts)
                    io.to(socket.id).emit(channels.agentAccounts, message);
            });

            subscriber.on('error', function (err) {
                console.log('Redis error: ' + err);
            });

            socket.on('disconnect', function () {
                console.log(socket.id + ' UnSub ');
                subscriber.quit();
            });
        }
        else
            console.log(socket.id + " authen fail");
    })
});

http.listen(config["port"], function () {
    console.log('listening on *:' + config["port"]);
});