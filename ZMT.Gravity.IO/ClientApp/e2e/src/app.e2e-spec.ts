
import { AppPage } from './app.po';

describe('GravityIO App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display application title: GravityIO', () => {
    page.navigateTo();
    expect(page.getAppTitle()).toEqual('GravityIO');
  });
});
