export const environment = {
  production: false,
  baseUrl: "http://192.168.0.113:5000",
  //baseUrl: "http://172.20.12.126:5000",
  loginUrl: "/login",
  bookieUrls: {
    PIN: 'ps38ag.com',
    SBO: 'https://agent.sbobet.com'
  },

  //nodeJS
  ip: 'localhost',
  port: 3001,
  authenNode: {
    inf: {
      id: "Gravity.IO",
      key: "daylapass"
    },
    authenChannel: {
      realTimeBets:"authenRealTimeBet",
      agentAccounts:"authenAgentAccount"
    }
  },
  channels: {
    test: "ChannelTest",
    realTimeBets: {
      newBet: "RealTimeBet",
      updatePT: "MemberPt",
      newViolation: "NewViolations"
    },
    agentAccounts: "AccountCaptcha"
  }
};
