export const environment = {
  production: true,
  baseUrl: "https://gravityio.app",
  //
  loginUrl: "/login",
  bookieUrls: {
    PIN: 'ps38ag.com',
    SBO: 'https://agent.sbobet.com'
  },

  //nodeJS
  ip: 'https://gravityio.app',
  port: 443,
  authenNode: {
    inf: {
      id: "Gravity.IO",
      key: "daylapass"
    },
    authenChannel: {
      realTimeBets:"authenRealTimeBet",
      agentAccounts:"authenAgentAccount"
    }
  },
  channels: {
    test: "ChannelTest",
    realTimeBets: {
      newBet: "RealTimeBet",
      updatePT: "MemberPt",
      newViolation: "NewViolations"
    },
    agentAccounts: "AccountCaptcha"
  }
};
