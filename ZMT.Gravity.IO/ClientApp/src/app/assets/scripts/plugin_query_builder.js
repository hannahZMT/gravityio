(function (global) {
  var QueryConverter = function () {
    this.settings = {
      default_condition: "AND",
      numberOfInputs: {
        "=": 1,
        "!=": 1,
        '<': 1,
        '<=': 1,
        '>': 1,
        '>=': 1,
        "is": 1,
        "not_equal": 1,
        "contains": 1,
        "less": 1,
        "less_or_equal": 1,
        "greater": 1,
        "greater_or_equal": 1
      }
      //in: 0,
      //not_in: 0,
      //less: 1,
      //less_or_equal: 1,
      //greater: 1,
      //greater_or_equal: 1,
      //between: 2,
      //not_between: 2,
      //begins_with: 1,
      //not_begins_with: 1,
      //contains: 1,
      //not_contains: 1,
      //ends_with: 1,
      //not_ends_with: 1,
      //is_empty: 0,
      //is_not_empty: 0,
      //is_null: 0,
      //is_not_null: 0
      ,
      escapeRegExp: function (str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
      },
      mongoOperators: {
        "=": function (v) { return v[0]; },
        "!=": function (v) { return { '$ne': v[0] }; },
        '<': function (v) { return { '$lt': v[0] }; },
        '<=': function (v) { return { '$lte': v[0] }; },
        '>': function (v) { return { '$gt': v[0] }; },
        '>=': function (v) { return { '$gte': v[0] }; },
        "is": function (v) { return v[0]; },
        "not_equal": function (v) { return { '$ne': v[0] }; },
        "contains": function (v) { return "/.*" + v[0] + ".*/i"; },
        "less": function (v) { return { '$lt': v[0] }; },
        "less_or_equal": function (v) { return { '$lte': v[0] }; },
        "greater": function (v) { return { '$gt': v[0] }; },
        "greater_or_equal": function (v) { return { '$gte': v[0] }; }
      }
      //in: function(v) { return { '$in': v }; },
      //not_in: function(v) { return { '$nin': v }; }, 
      //less: function(v) { return { '$lt': v[0] }; },
      //less_or_equal: function(v) { return { '$lte': v[0] }; },
      //greater: function(v) { return { '$gt': v[0] }; },
      //greater_or_equal: function(v) { return { '$gte': v[0] }; },
      //between: function(v) { return { '$gte': v[0], '$lte': v[1] }; },
      //not_between: function(v) { return { '$lt': v[0], '$gt': v[1] }; },
      //begins_with: function(v) { return { '$regex': '^' + escapeRegExp(v[0]) }; },
      //not_begins_with: function(v) { return { '$regex': '^(?!' + escapeRegExp(v[0]) + ')' }; },
      //contains: function(v) { return { '$regex': escapeRegExp(v[0]) }; },
      //not_contains: function(v) { return { '$regex': '^((?!' + escapeRegExp(v[0]) + ').)*$', '$options': 's' }; },
      //ends_with: function(v) { return { '$regex': escapeRegExp(v[0]) + '$' }; },
      //not_ends_with: function(v) { return { '$regex': '(?<!' + escapeRegExp(v[0]) + ')$' }; },
      //is_empty: function(v) { return ''; },
      //is_not_empty: function(v) { return { '$ne': '' }; },
      //is_null: function(v) { return null; },
      //is_not_null: function(v) { return { '$ne': null }; }
    };

    this.getMongoQuery = function (data) {
      if (!data) {
        return null;
      }

      var self = this;

      return (function parse(group) {
        if (!group.condition) {
          group.condition = self.settings.default_condition;
        }
        if (['AND', 'OR'].indexOf(group.condition.toUpperCase()) === -1) {
          console.log("Unable to build MongoDB query with condition " + group.condition);
        }

        if (!group.rules) {
          return {};
        }

        var parts = [];

        group.rules.forEach(function (rule) {
          if (rule.rules && rule.rules.length > 0) {
            parts.push(parse(rule));
          } else {
            var mdb = self.settings.mongoOperators[rule.operator];
            var ope = self.settings.numberOfInputs[rule.operator];

            if (mdb === undefined) {
              console.log('Unknown MongoDB operation for operator ' + rule.operator);
            }

            var value = rule.value;

            if (ope !== 0) {
              if (!(rule.value instanceof Array)) {
                value = [rule.value];
              }
            }

            var field = rule.field;
            var ruleExpression = {};

            ruleExpression[field] = mdb.call(self, value);
            parts.push(ruleExpression);
          }
        });

        var groupExpression = {};
        groupExpression['$' + group.condition.toLowerCase()] = parts;

        return groupExpression;
      }(data));
    };
  }

  module.exports = new QueryConverter();
})(window);
