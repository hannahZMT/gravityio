import { RealTimeBetView } from "./realtimebet.model";

export class Task {
    task: (obj: RealTimeBetView) => boolean;
    periodTime: number;

    constructor(
        task: (obj: RealTimeBetView) => boolean
        , periodTime: number
    ) {
        this.task = task;
        this.periodTime = periodTime;
    }
}