import { MemberRiskTagResponse, MemberRiskTagDb, MemberRiskTagView } from "./member-risk-tag.model";
import { Bookie, BetSelection, OddsType } from "./enums";
import { formatDate } from "@angular/common";
import { isNullOrUndefined } from "@swimlane/ngx-datatable/release/utils";
import { IconStyle } from "./bet.model";

export class RealTimeBetView {
  matchId: number;
  league: string;
  homeName: string;
  awayName: string;
  userNameCode: string;
  type: string;
  betSelection: string;
  cover: string;
  market: string;
  hdpOrGoal: number;
  bookieRef: string;
  ipAddress: string;
  stake: number;
  eventDate: string;
  betDate: string;
  odds: number;
  euroOdds: number;
  lastOdds: number;
  deltaOdds: number[];
  score: string;
  sport: string;
  status: string;
  currencyCode: string;
  resultFT: string;

  id: string;
  bookie: number;
  sportsType: number;
  oddsType: OddsType;
  betStatus: string;
  countryCode: string;
  isSettled: boolean;
  isCancelled: boolean;
  isCalculated: boolean;

  memberRiskTag: MemberRiskTagView;
  timeZone: string;


  constructor(data: RealTimeBetDb, obj?: RealTimeBetResponse, timeZone?: string) {
    if (timeZone)
      this.timeZone = timeZone;
    if (isNullOrUndefined(data)) {
      for (var key in obj)
        this[key] = obj[key];
      this.memberRiskTag = new MemberRiskTagView(obj.memberRiskTag);
      this.score = isNullOrUndefined(obj.score) ? "" : obj.score + " ";
    }
    else {
      for (var key in data) {
        var tkey = String.fromCharCode(key.charCodeAt(0) + (97 - 65)) + key.substring(1, key.length);
        this[tkey] = data[key];
      }

      if (data.MemberRiskTag) {
        var tmp: MemberRiskTagResponse = {
          memberFullName: data.MemberRiskTag.MemberFullName
          , userName: data.MemberRiskTag.UserName
          , memberPt: data.MemberRiskTag.MemberPt
          , positionTagValue: data.MemberRiskTag.PositionTagValue
          , source: data.MemberRiskTag.Source
        }
        this.memberRiskTag = new MemberRiskTagView(tmp);
      }
      this.score = isNullOrUndefined(data.Score) ? "" : data.Score + " ";
    }
  }

  get sourceMemberCellElement(): HTMLTableDataCellElement {
    var cell = document.createElement("td");
    var div: HTMLDivElement[] = [undefined
      , document.createElement("div")
      , document.createElement("div")
      , document.createElement("div")
      , document.createElement("div")
    ];

    cell.insertBefore(div[1], undefined);
    cell.insertBefore(div[2], undefined);
    cell.insertBefore(div[3], undefined);
    cell.insertBefore(div[4], undefined);

    {
      let a: HTMLAnchorElement = document.createElement("a");

      div[1].insertBefore(a, undefined);

      a.innerText = this.userNameCode;
      a.href = "/bets-history?userNameCode=" + this.userNameCode;
      a.target = "_blank";
    }

    {
      let a: HTMLAnchorElement = document.createElement("a");
      let b = document.createElement("b");

      div[2].insertBefore(a, undefined);
      a.insertBefore(b, undefined);

      b.innerText = this.memberRiskTag != null ? this.memberRiskTag.memberFullName : '';
      a.href = "/bets-history?fullName=" + b.innerText;
      a.target = "_blank";
    }

    const countryCode = isNullOrUndefined(this.countryCode) ? "" : '(' + this.countryCode + ') ';
    div[3].innerText = countryCode + this.ipAddress;

    if (this.memberRiskTag && this.memberRiskTag.memberPt) {
      div[4].innerText = this.memberRiskTag.memberPt;
      div[4].setAttribute("class", this.memberRiskTag.css_memberPt);
    }
    return cell;
  }

  get betDetailsCellElement(): HTMLTableDataCellElement {
    var cell = document.createElement("td");
    var div: HTMLDivElement[] = [undefined
      , document.createElement("div")
      , document.createElement("div")
      , document.createElement("div")
      , document.createElement("div")];

    cell.insertBefore(div[1], undefined);
    cell.insertBefore(div[2], undefined);
    cell.insertBefore(div[3], undefined);
    cell.insertBefore(div[4], undefined);

    {
      let b = document.createElement("b");
      let a = document.createElement("a");

      div[1].insertBefore(b, undefined);
      div[1].insertBefore(a, undefined);

      b.innerText = Bookie[this.bookie] + ' ';

      a.innerText = this.bookieRef;
      a.href = "/bets-history?bookieRef=" + this.bookieRef;
      a.target = "_blank";
    }

    {
      let b = document.createElement("b");
      b.innerText = this.hdpOrGoal.toString() + ' ';
      div[2].insertBefore(b, undefined);

      div[2].innerText = this.type;

      b = document.createElement("b");
      b.innerText = '@' + this.betSelection;
      div[2].insertBefore(b, undefined);
    }

    {
      let b = document.createElement("b");
      b.innerText = '@' + this.odds;

      div[3].innerText = this.cover + ' ' + this.score;
      div[3].insertBefore(b, undefined);
    }
    div[4].innerText = formatDate(this.betDate, 'dd/MM/yyy HH:mm:ss', 'en', this.timeZone);

    return cell;
  }

  get matchDetailsCellElement(): HTMLTableDataCellElement {
    var cell = document.createElement("td");
    var div: HTMLDivElement[] = [undefined
      , document.createElement("div")
      , document.createElement("div")
      , document.createElement("div")];

    cell.insertBefore(div[1], undefined);
    cell.insertBefore(div[2], undefined);
    cell.insertBefore(div[3], undefined);

    {
      let a: HTMLAnchorElement = document.createElement("a");
      let span: HTMLSpanElement = document.createElement("span");

      div[1].insertBefore(a, undefined);
      div[1].insertBefore(span, undefined);

      a.innerText = this.matchId.toString();
      a.href = "/bets-history?matchId=" + this.matchId.toString();
      a.target = "_blank";
      span.innerText = " " + this.league;
    }

    {
      let span: HTMLSpanElement[] = [undefined
        , document.createElement("span")
        , document.createElement("span")
        , document.createElement("span")];

      div[2].insertBefore(span[1], undefined);
      span[1].innerText = this.homeName;
      if (this.betSelection == BetSelection[BetSelection.HOME]) span[1].setAttribute("class", "selection-team");

      if (!isNullOrUndefined(this.awayName)) {
        div[2].insertBefore(span[2], undefined);
        div[2].insertBefore(span[3], undefined);

        span[2].innerText = " vs ";
        span[3].innerText = this.awayName;

        if (this.betSelection == BetSelection[BetSelection.AWAY]) span[3].setAttribute("class", "selection-team");
      }
    }

    div[3].innerText = formatDate(this.eventDate, 'dd/MM/yyy HH:mm:ss', 'en', this.timeZone);

    return cell;
  }

  get stakeCellElement(): HTMLTableDataCellElement {
    var cell = document.createElement("td");
    var div: HTMLDivElement[] = [undefined
      , document.createElement("div")
      , document.createElement("div")];

    cell.insertBefore(div[1], undefined);
    cell.insertBefore(div[2], undefined);

    div[1].innerText = isNullOrUndefined(this.stake) ? "" : this.stake.toString();
    div[2].innerText = this.currencyCode;

    return cell;
  }

  get placedOddsCellElement(): HTMLTableDataCellElement {
    var cell = document.createElement("td");
    var div: HTMLDivElement[] = [undefined, document.createElement("div")];

    cell.insertBefore(div[1], undefined);

    const euroOdds = this.euroOdds ? (Math.round(this.euroOdds * 1000) / 1000).toString() : '';
    if (this.oddsType != OddsType.EU) {
      let odds = (!isNullOrUndefined(this.odds) && this.oddsType == OddsType.AM
        ? this.odds
        : (!isNullOrUndefined(this.odds) && this.oddsType != OddsType.AM
          ? Math.round(this.odds * 1000) / 1000
          : '')).toString();

      div[1].innerText = odds + ' ' + OddsType[this.oddsType];

      div[2] = document.createElement("div");
      cell.insertBefore(div[2], undefined);
      div[2].innerText = euroOdds;
    }
    else
      div[1].innerText = euroOdds;

    return cell;
  }

  public static lastOddsCellElement(lastOdds: number, euroOdds: number, market: string, oddsType: OddsType, odds: number): HTMLTableDataCellElement {
    var cell = document.createElement("td");
    var iElement = document.createElement("i");
    var divElement = document.createElement("div");

    const iconStyle: IconStyle = new IconStyle(lastOdds, euroOdds);

    if (lastOdds == 0) {
      iElement.setAttribute("class", iconStyle.icon.prefix + ' fa-' + iconStyle.icon.iconName);
      iElement.setAttribute("style", "color:" + iconStyle.style);
      cell.insertBefore(iElement, undefined);
    } else {
      divElement.innerText = isNullOrUndefined(lastOdds) ? "_" : lastOdds.toString();
      cell.insertBefore(divElement, undefined);

      if (market == "PreLive" && lastOdds != null) {
        var div = document.createElement("div")
        div.innerText = "DROP!!!";
        div.setAttribute("class", "DROP");

        if (euroOdds && lastOdds < euroOdds)
          cell.insertBefore(div, undefined);
        if (!euroOdds && oddsType == OddsType.EU && lastOdds < odds)
          cell.insertBefore(div, undefined);
      }
    }

    return cell;
  }

  public static deltaOddDivElement(deltaOdd: number, euroOdds: number): HTMLDivElement {
    var div: HTMLDivElement = document.createElement("div");
    const iconStyle: IconStyle = new IconStyle(deltaOdd, euroOdds);

    const d = isNullOrUndefined(deltaOdd) ? "_" : deltaOdd;
    if (d != 0) {
      let span: HTMLSpanElement = document.createElement("span");
      div.insertBefore(span, undefined);
      span.innerText = d.toString() + ' ';
    }

    if (iconStyle.icon) {
      let iElement: HTMLElement = document.createElement("i");
      div.insertBefore(iElement, undefined);
      iElement.setAttribute("class", iconStyle.icon.prefix + ' fa-' + iconStyle.icon.iconName);
      iElement.setAttribute("style", "color:" + iconStyle.style);
    }

    return div;
  }

  get deltaOddsCellElement(): HTMLTableDataCellElement {
    var cell = document.createElement("td");

    cell.insertBefore(RealTimeBetView.deltaOddDivElement(this.deltaOdds[0], this.euroOdds), undefined);
    cell.insertBefore(RealTimeBetView.deltaOddDivElement(this.deltaOdds[1], this.euroOdds), undefined);
    cell.insertBefore(RealTimeBetView.deltaOddDivElement(this.deltaOdds[2], this.euroOdds), undefined);

    return cell;
  }

  get marketCellElement(): HTMLTableDataCellElement {
    var cell = document.createElement("td");
    var div: HTMLDivElement[] = [undefined
      , document.createElement("div")
      , document.createElement("div")
    ]

    cell.insertBefore(div[1], undefined);
    cell.insertBefore(div[2], undefined);

    div[1].innerText = this.market;
    div[1].setAttribute("class", this.market);

    div[2].innerText = this.sport;

    return cell;
  }

  get matchStatusElement(): HTMLTableDataCellElement {
    var cell = document.createElement("td");
    var div: HTMLDivElement[] = [undefined
      , document.createElement("div")
      , document.createElement("div")
    ]

    cell.insertBefore(div[1], undefined);
    cell.insertBefore(div[2], undefined);

    div[1].innerText = this.status;
    div[1].setAttribute("class", this.status);

    div[2].innerText = this.resultFT;

    return cell;
  }

  get getRowElement(): HTMLTableRowElement {
    var tr: HTMLTableRowElement = document.createElement('tr');
    var tds: HTMLTableDataCellElement[] = [];

    tr.setAttribute("r.id", this.id)
    tr.setAttribute("r.betDate", this.betDate);

    tds[0] = this.sourceMemberCellElement;
    tds[1] = this.betDetailsCellElement;
    tds[2] = this.matchDetailsCellElement;
    tds[3] = this.stakeCellElement;
    tds[4] = this.placedOddsCellElement;
    tds[5] = RealTimeBetView.lastOddsCellElement(this.lastOdds, this.euroOdds, this.market, this.oddsType, this.odds);
    tds[6] = this.deltaOddsCellElement;
    tds[7] = this.marketCellElement;
    tds[8] = this.matchStatusElement;

    for (var i = 0; i <= 8; i++)
      tr.insertBefore(tds[i], undefined);

    return tr;
  }
}

export class RealTimeBetDb {
  MatchId: number;
  League: string;
  HomeName: string;
  AwayName: string;
  UserNameCode: string;
  Type: string
  BetSelection: string
  Cover: string;
  Market: string;
  HdpOrGoal: number;
  BookieRef: string;
  IpAddress: string;
  Stake: number;
  EventDate: string;
  BetDate: string;
  Odds: number;
  EuroOdds: number;
  LastOdds: number;
  DeltaOdds: number[];
  Score: string;
  Sport: string;
  Status: string;
  CurrencyCode: string;
  ResultFT: string;

  Id: string;
  Bookie: Bookie;
  SportsType: number;
  OddsType: number;
  BetStatus: string;
  CountryCode: string;
  IsSettled: boolean;
  IsCancelled: boolean;
  IsCalculated: boolean;

  MemberRiskTag: MemberRiskTagDb;
}

export interface RealTimeBetResponse {
  matchId: number;
  league: string;
  homeName: string;
  awayName: string;
  userNameCode: string;
  type: string;
  betSelection: string;
  cover: string;
  market: string;
  hdpOrGoal: number;
  bookieRef: string;
  ipAddress: string;
  stake: number;
  eventDate: string;
  betDate: string;
  odds: number;
  euroOdds: number;
  lastOdds: number;
  deltaOdds: number[];
  score: string;
  sport: string;
  status: string;
  currencyCode: string;
  resultFT: string;

  id: string;
  bookie: number;
  sportsType: number;
  oddsType: number;
  betStatus: string;
  countryCode: string;
  isSettled: boolean;
  isCancelled: boolean;
  isCalculated: boolean;

  memberRiskTag: MemberRiskTagResponse;
}
