import { SmartRuleSeverity } from "./enums";
import { formatDate } from "@angular/common";
import { Setting } from "./smart-rule-settings.model";
import { isNullOrUndefined } from "util";

export interface SmartRuleResponse {
  ruleId: string;
  ruleName: string;
  userName: string;
  lastedUpdate: Date;
  isActive: boolean;
  severity: SmartRuleSeverity;

  createDate: Date;
  settings: Setting[];
  note: string;
}

export class SmartRuleView {
  ruleId: string;
  ruleName: string;
  userName: string;
  lastedUpdate: string;
  isActive: boolean;
  severity: string;
  note: string; //use by alert-violations

  css_severity: string;

  constructor(res: SmartRuleResponse, timeZone: string) {
    if (!res) return;
    this.ruleId = res.ruleId;
    this.ruleName = res.ruleName;
    this.userName = res.userName;
    this.lastedUpdate = res.lastedUpdate ? formatDate(res.lastedUpdate, 'dd/MM/yyy HH:mm:ss', 'en', timeZone) : undefined;
    this.isActive = res.isActive;
    this.severity = SmartRuleSeverity[res.severity];
    this.note = res.note; //use by alert-violations

    this.css_severity = "severity-box-" + SmartRuleSeverity[res.severity];
  }
}

export class SmartRuleEdit {
  readonly ruleId: string;
  ruleName: string;
  isActive: boolean;
  severity: SmartRuleSeverity;
  settings: Setting[];
  note: string;

  get css_severity(): string {
    return "severity-text-" + SmartRuleSeverity[this.severity];
  }

  constructor(obj: SmartRuleResponse) {
    if (!obj) return;

    this.ruleId = isNullOrUndefined(obj.ruleId) ? undefined : obj.ruleId;
    this.ruleName = obj.ruleName;
    this.isActive = obj.isActive;
    this.severity = obj.severity;
    this.settings = obj.settings;
    this.note = obj.note;
  }
}
