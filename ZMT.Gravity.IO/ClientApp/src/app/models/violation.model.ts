import { ViolationStatus, SmartRuleSeverity, Bookie } from "./enums";
import { ViolationDetail } from "./violation-detail.model";
import { formatDate } from "@angular/common";
import { isNullOrUndefined } from "util";
import { MemberRiskTagResponse, MemberRiskTagView } from "./member-risk-tag.model";


//Violations
export interface ViolationResponse {
    id: string;
    ruleId: string;
    ruleName: string;
    message: string;
    details: ViolationDetails;
    status: ViolationStatus;
    severity: SmartRuleSeverity;
    notes: string;
    createdTime: Date;
    reviewedTime: Date;
    reviewedBy: string;

    memberRiskTag: MemberRiskTagResponse;
}
export interface ViolationDetails {
    userCode: string;
    source: number;
    data: string;
    dataType: number;
}

export class ViolationView {
    id: string;
    ruleId: string;
    ruleName: string;
    message: string;
    details: ViolationDetail;
    status: string;
    severity: string;
    notes: string;
    createdTime: string;
    reviewedTime: string;
    reviewedBy: string;
    memberRiskTag: MemberRiskTagView;

    css_severity: string;

    constructor(res: ViolationResponse, timeZone: string) {
        this.id = res.id;
        this.ruleId = res.ruleId;
        if (res.details) this.details = new ViolationDetail(res.details, timeZone);
        this.createdTime = res.createdTime ? formatDate(res.createdTime, 'dd/MM/yyy HH:mm:ss', 'en', timeZone) : undefined;
        this.ruleName = res.ruleName;
        this.message = res.message;
        this.severity = SmartRuleSeverity[res.severity];
        this.status = ViolationStatus[res.status];
        this.reviewedTime = res.reviewedTime ? formatDate(res.reviewedTime, 'dd/MM/yyy HH:mm:ss', 'en', timeZone) : undefined;
        this.reviewedBy = res.reviewedBy;
        this.notes = res.notes;
        this.memberRiskTag = new MemberRiskTagView(res.memberRiskTag);

        this.css_severity = "severity-box-" + this.severity;
    }

    static getExportData(rowsView: ViolationView[]) {
        var data = [];

        for (var i = 0; i < rowsView.length; i++) {
            data.push({
                col1: rowsView[i].createdTime
                , col2: rowsView[i].ruleId
                , col3: rowsView[i].memberRiskTag.source
                , col4: rowsView[i].memberRiskTag.userName
                , col5: rowsView[i].memberRiskTag.memberFullName
                , col6: rowsView[i].message
                , col7: rowsView[i].memberRiskTag.memberPt
                , col8: rowsView[i].severity
                , col9: rowsView[i].status
                , col10: rowsView[i].reviewedBy
                , col11: rowsView[i].reviewedTime
                , col12: rowsView[i].notes
            })
            for (var key in data[i])
                if (isNullOrUndefined(data[i][key]))
                    data[i][key] = "";
        }

        return data;
    }
    static get exportHeader(): string[] {
        return ["Create time", "Rule id", "Source", "Usercode", "Message", "Severity", "Status", "Reviewed by", "Reviewed time", "Notes"];
    }
}

//RealTimeBet
export interface ViolationDb {
    Id: string;
    RuleId: string;
    RuleName: string;
    Message: string;
    //details: string;
    Status: ViolationStatus;
    Severity: SmartRuleSeverity;
    Notes: string;
    CreatedTime: Date;
    ReviewedTime: Date;
    ReviewedBy: string;
}