export class RiskTaggingField {
    prop: string;
    name: string;
    show: boolean;

    constructor(prop: string, show: boolean) {
        this.prop = prop;
        this.name = RiskTaggingField.propToName(this.prop);
        this.show = show;
    }

    static get props() {
        return [
            "fullName", "currency", "country", "kyc", "bonus", "dateOfBirth", "dateOfStart"
            , "positionTagName", "status", "firstDepEurBtc", "firstDepGbp"
            , "pinBet", "maxBet", "sboBet", "skypeBet", "singBet", "betIsn"
            , "matchBook", "aoEu", "aoGb", "ga288", "nightWickets", "orbEu", "userNameBookies"
        ]
    }
    static get names() {
        return [
            "Full Name", "Currency", "Country", "KYC", "Bonus", "Date of Birth", "Date of Start"
            , "Position Tag", "Status", "1st Dep.(EUR/BTC)", "1st Dep.(GBP)"
            , "PINBET", "MAXBET", "SBOBET", "SKYPE BETTING", "SINGBET", "BETISN"
            , "MATCHBOOK", "AO(EU)", "AO(GB)", "GA288", "9WICKETS", "Orb(EU)", "Username Bookies"]
    }
    static propToName(prop: string): string {
        for (var i = 0; i < this.props.length; i++)
            if (this.props[i] == prop)
                return this.names[i];
        if (prop == "positionTagValue") return "Position Tag"
        return undefined;
    }

    static valueSendApi(fields: RiskTaggingField[]) {
        var data: {} = {};
        for (var i = 0; i < fields.length; i++) {
            if (fields[i].prop == "positionTagName")
                data["positionTagValue"] = fields[i].show;
            else
                data[fields[i].prop] = fields[i].show;
        }
        return data;
    }
}