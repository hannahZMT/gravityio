import { isNullOrUndefined } from "util";
import { formatNumber } from "@angular/common";

export class Alg {
    public static isNumber(str: string): boolean {
        if (isNullOrUndefined(str)) return false;
        if (str.length == 0) return false;

        for (var i = 0; i < str.length; i++) {
            const num = Number.parseInt(str[i]);
            if (isNaN(num)) return false;
        }
        return true;
    }

    public static get nowString(): string {
        const currentDate = new Date();
        return currentDate.getFullYear()
            + formatNumber(currentDate.getMonth() + 1, "en", "2.0-0")
            + formatNumber(currentDate.getDate(), "en", "2.0-0") + "_"
            + formatNumber(currentDate.getHours(), "en", "2.0-0")
            + formatNumber(currentDate.getMinutes(), "en", "2.0-0")
            + formatNumber(currentDate.getSeconds(), "en", "2.0-0")
    }

    public static rem2Space(val: string): string {
        while (val.indexOf('  ') > 0)
            val = val.replace('  ', ' ');
        return val;
    }

    public static transEnum(val: string) {
        if (!val || !val[0]) return "";

        let res: string = val.substring(1, val.length);
        let needRep = {};
        for (let i = 0; i < res.length; i++)
            if (val[i] == val[i].toUpperCase())
                if (needRep[val[i]])
                    needRep[val[i]]++
                else
                    needRep[val[i]] = 1;

        for (let chr in needRep)
            res = res.replace(chr, ' ' + chr);

        return val[0] + res;
    }
}