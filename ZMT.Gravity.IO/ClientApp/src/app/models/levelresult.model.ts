export class LevelResult {
  level: string;
  ptPercent: number;
  ptResult: number;
  totalResult: number;
}
