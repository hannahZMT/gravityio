export class UserSetting {
    isServerTime: boolean;
    theme: string;
}