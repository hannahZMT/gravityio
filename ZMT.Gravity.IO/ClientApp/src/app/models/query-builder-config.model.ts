import { QueryBuilderConfig } from "angular2-query-builder";
import { isUndefined, isNull } from "util";
import { SearchDate } from "./search.model";

export interface QueryRules {
  field: string;
  operator: string;
  value: any;
}

export class QueryBuilder {
  condition: string;
  rules: QueryRules[];

  private get sports() {
    return [
      { name: 'Soccer', value: 'Soccer' },
      { name: 'Baseball', value: 'Baseball' },
      { name: 'Basketball', value: 'Basketball' },
      { name: 'E Sports', value: 'E Sports' },
      { name: 'Tennis', value: 'Tennis' }
    ]
  }
  get config(): QueryBuilderConfig {
    return {
      fields: {
        Sport: {
          name: 'Sport',
          type: 'category',
          operators: ['is'],
          options: this.sports
        },
        UserNameCode: { name: 'Member', type: 'string', operators: ['is', 'contains'] },
        Stake: { name: 'Stake', type: 'number' },
        Cover: {
          name: 'Cover',
          type: 'category',
          operators: ['is'],
          options: [
            { name: 'FullTime', value: 'FT' },
            { name: 'HalfTime', value: 'HT' }
          ]
        },
        Type: {
          name: 'Type',
          type: 'category',
          operators: ['is'],
          options: [
            { name: 'Over/Under', value: 'OU' },
            { name: 'Handicap', value: 'HDP' },
            { name: '1x2', value: '1x2' }
          ]
        },
        Market: {
          name: 'Market',
          type: 'category',
          operators: ['is'],
          options: [
            { name: 'Live', value: 'Live' },
            { name: 'PreLive', value: 'PreLive' }
          ]
        },
        BetSelection: {
          name: 'Selection',
          type: 'category',
          operators: ['is'],
          options: [
            { name: 'Home', value: 'HOME' },
            { name: 'Away', value: 'AWAY' },
            { name: 'Over', value: 'OVER' },
            { name: 'Under', value: 'UNDER' }
          ]
        },
        HDPOrGoal: { name: 'HDP/Goal', type: 'number' },
        MatchId: { name: 'MatchId', type: 'number', operators: ['='] },
        League: { name: 'League', type: 'string', operators: ['is', 'contains'] },
        HomeName: { name: 'Home Team', type: 'string', operators: ['is', 'contains'] },
        AwayName: { name: 'Away Team', type: 'string', operators: ['is', 'contains'] },
        EventDate: { name: 'Event Date', type: 'date', defaultValue: (() => new Date()), },
        BetDate: { name: 'Bet Date', type: 'date', defaultValue: (() => new Date()), },
        IP: { name: 'Ip Address', type: 'string', operators: ['is', 'contains'] },
        BookieRef: { name: 'Bookie Ref', type: 'string', operators: ['is', 'contains'] }
      }
    }
  }

  constructor(dateField: "BetDate" | "EventDate", sportsType?: number, fromDate?: Date | string, toDate?: Date | string) {
    this.condition = "and";

    this.rules = [];
    if (!isNull(sportsType) && sportsType.toString() != "null")
      this.rules.push({ field: 'SportsType', operator: 'is', value: sportsType });
    if (!isUndefined(fromDate))
      this.rules.push({ field: dateField, operator: '>=', value: fromDate });
    if (!isUndefined(toDate))
      this.rules.push({ field: dateField, operator: '<=', value: toDate });
  }

  queryWithFormatDate(dateField: "BetDate" | "EventDate", dateFields: string[], timeZone: string): QueryBuilder {
    var query = new QueryBuilder(dateField);

    for (var i = 0; i < this.rules.length; i++) {
      query.rules.push({
        field: this.rules[i].field
        , operator: this.rules[i].operator
        , value: this.rules[i].value
      });

      for (var j = 0; j < dateFields.length; j++) {
        if (query.rules[i].field == dateFields[j] && query.rules[i].operator == ">=") {
          var fixDate = new SearchDate(new Date(query.rules[i].value), null, timeZone);
          query.rules[i].value = fixDate.fromDate;
        }
        if (query.rules[i].field == dateFields[j] && query.rules[i].operator == "<=") {
          var fixDate = new SearchDate(null, new Date(query.rules[i].value), timeZone);
          query.rules[i].value = fixDate.toDate;
        }
      }
    }

    return query;
  }
}