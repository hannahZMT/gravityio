export class AuditValue {
  columnName: string;
  oldValue: string;
  newValue: string;

  constructor(column?: string, oldV?: string, newV?: string) {
    this.columnName = column;
    this.oldValue = oldV;
    this.newValue = newV;
  }

  public static parseAudit(obj: AuditValue[]): string[] {
    var filter: string[] = []
    for (var i = 0; i < obj.length; i++)
      if (obj[i].columnName != 'Settings')
        if (obj[i].oldValue !== obj[i].newValue)
          filter.push(obj[i].columnName + " : " + obj[i].oldValue + " ==> " + obj[i].newValue);
        else;
      else {
        const oldVal: Setting[] = JSON.parse(obj[i].oldValue);
        const newVal: Setting[] = JSON.parse(obj[i].newValue);
        //old.length == new.length

        for (var j = 0; j < oldVal.length; j++) {
          var tmp: { label: string, prop: string, oldVal: any, newVal: any }[] = [];
          if (oldVal[j].Active !== newVal[j].Active)
            tmp.push({ label: oldVal[j].Label, prop: 'is active', oldVal: oldVal[j].Active, newVal: newVal[j].Active });
          if (oldVal[j].Operator.Select !== newVal[j].Operator.Select)
            tmp.push({ label: oldVal[j].Label, prop: 'operator', oldVal: oldVal[j].Operator.Select, newVal: newVal[j].Operator.Select });
          if (this.checkValuesChange(oldVal[j].Value, newVal[j].Value))
            tmp.push({ label: oldVal[j].Label, prop: 'value', oldVal: oldVal[j].Value, newVal: newVal[j].Value });

          if (tmp.length > 0) {
            var str: string = oldVal[j].Label + " : ";
            for (var k = 0; k < tmp.length; k++)
              str += (tmp[k].prop + " : " + tmp[k].oldVal + " ==> " + tmp[k].newVal + ' ; ');
            filter.push(str);
          }
        }

      }
    return filter;
  }

  public static checkValuesChange(oldVal: any[], newVal: any[]): boolean {
    if (oldVal.length != newVal.length)
      return true;

    for (var i = 0; i < oldVal.length; i++)
      if (oldVal[i] != newVal[i])
        return true;

    return false;
  }
}

export interface Setting {
  
  Label: string;
  Name: string;
  Operator: Operator;
  Type: 'number' | 'string' | 'date' | 'checkbox';
  Value: any[];
  Active: boolean;
}

export interface Operator {
  Select: string;
  Data: string[];
}