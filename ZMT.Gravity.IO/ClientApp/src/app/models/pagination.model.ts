export class PageSearch {
    index: number = 1;
    size: number = 500;
}

export class PaginationView {
    maxSize: number = 9;
    totalPages: number;
    totalItems: number;
}