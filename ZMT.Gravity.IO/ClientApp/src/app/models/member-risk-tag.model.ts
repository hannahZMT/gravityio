import { Bookie } from "./enums";
import { isNullOrUndefined } from "@swimlane/ngx-datatable/release/utils";
import { isNull } from "util";

export interface MemberRiskTagResponse {
  memberFullName: string;
  userName: string;
  memberPt: string;
  positionTagValue: number;
  source: Bookie;
}

export interface MemberRiskTagDb {
  MemberFullName: string;
  UserName: string;
  MemberPt: string;
  PositionTagValue: number;
  Source: Bookie;
}

export class MemberRiskTagView {
  memberFullName: string;
  userName: string;
  memberPt: string;
  source: string;
  positionTagValue: number;

  css_memberPt: string;

  constructor(res: MemberRiskTagResponse) {
    this.memberFullName = isNullOrUndefined(res) || isNullOrUndefined(res.memberFullName) ? "Noname" : res.memberFullName;
    this.userName = isNullOrUndefined(res) || isNullOrUndefined(res.userName) ? "" : res.userName;
    this.memberPt = isNullOrUndefined(res) || isNullOrUndefined(res.memberPt) ? "" : res.memberPt;
    this.source = isNullOrUndefined(res) || isNullOrUndefined(res.source) ? "" : Bookie[res.source];
    this.positionTagValue = isNullOrUndefined(res) || isNullOrUndefined(res.positionTagValue) ? null : res.positionTagValue;

    this.css_memberPt = "pt-div pt-div-" + this.memberPt.replace(/\s+/g, '');
  }
}