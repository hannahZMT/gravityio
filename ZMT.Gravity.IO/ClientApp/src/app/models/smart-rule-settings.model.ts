export interface Operator {
  select: string;
  data: string[];
}

export interface Setting {
  label: string;
  name: string;
  operator: Operator;
  type: 'number' | 'string' | 'date' | 'checkbox' | 'ips';
  value: any[];
  active: boolean;
}