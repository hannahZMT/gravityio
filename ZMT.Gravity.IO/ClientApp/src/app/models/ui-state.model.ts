export interface UIState {
  init?: boolean;
  loading?: boolean;
  data?: boolean;

  errors?: boolean;
  exportAll?: boolean;
  exportCur?: boolean;

  import?: boolean;
  edit?: boolean;

  saving?: boolean;
}