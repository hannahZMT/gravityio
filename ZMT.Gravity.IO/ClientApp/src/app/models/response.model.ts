export interface ApiResponse<T> {
    codeMessage: ErrorResponse;
    data: T;
}

export interface ErrorResponse {
    code: number;
    message: string;
}

export interface Pagging<T> {
    totalItems: number;
    totalPages: number;
    currentPage: number;
    pageSize: number;
    startPage: number;
    endPage: number;
    data: T;
  }
  