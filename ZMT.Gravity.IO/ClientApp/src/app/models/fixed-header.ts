import { Subscription, timer } from "rxjs";

export class FixedHeader {
    tableName: string;
    check: boolean;
    timers: Subscription[] = [];

    oldTopPosition: number;
    oldLeftPostion: number;
    widths: string[] = [];
    min_widths: string[] = [];

    thead: HTMLElement;
    ths: HTMLCollectionOf<HTMLTableHeaderCellElement>;
    tbody: HTMLElement;

    constructor(tableName: string) {
        this.tableName = tableName;
        this.check = false;
    }

    addEventListener() {
        window.addEventListener('scroll', this.scroll, true);
        window.addEventListener('resize', this.resize, true);
    }

    removeEventListener() {
        window.removeEventListener('scroll', this.scroll, true);
        window.removeEventListener('resize', this.resize, true);

        for (var i = 0; i < this.timers.length; i++)
            if (!this.timers[i].closed)
                this.timers[i].unsubscribe();
    }

    scroll = (): void => {
        if (!this.check) return;
        console.log("scroll");

        this.updateLeftHeader();

        const topPos = this.tbody.getBoundingClientRect().top - 50;
        if (this.oldTopPosition * topPos < 0) {
            if (topPos < 0) {
                this.updateWidthHeader(true);
                this.thead.style.position = "fixed";
                this.thead.style.display = "";
            }
            else {
                this.thead.style.position = "";
                this.updateWidthHeader(false);
                this.thead.style.display = "none";
            }

            this.oldTopPosition = topPos;
        }
    }

    updateLeftHeader() {
        const leftPos = this.tbody.getBoundingClientRect().left;
        if (leftPos != this.oldLeftPostion) {
            this.thead.style.left = leftPos + "px";
            this.oldLeftPostion = leftPos;
        }
    }

    resize = (): void => {
        if (!this.check) return;
        console.log("resize");
        this.getWidthForHeader(true);
        this.updateLeftHeader();
    }

    updateWidthHeader(newWidth: boolean) {
        console.log("update width " + newWidth);

        for (var i = 0; i < this.ths.length; i++) {
            this.ths[i].style.width = newWidth ? this.widths[i] : "";
            this.ths[i].style.minWidth = newWidth ? this.widths[i] : this.min_widths[i];
        }
    }

    init() {
        this.thead = $(this.tableName + ">thead")[0];
        this.tbody = $(this.tableName + ">tbody")[0];
        this.ths = this.thead.children[0].getElementsByTagName("th") as HTMLCollectionOf<HTMLTableHeaderCellElement>;

        this.oldTopPosition = this.tbody.getBoundingClientRect().top - 50;
        this.oldLeftPostion = this.tbody.getBoundingClientRect().left;
    }

    getWidthForHeader(updateHeader?: boolean) {
        if (!this.check) return;

        this.timers.push(timer(5)
            .subscribe(() => {
                const ths = $(this.tableName + '>tbody>tr>td');
                for (var i = 0; i < this.ths.length; i++) {
                    this.widths[i] = getComputedStyle(ths[i]).width;
                    this.min_widths[i] = getComputedStyle(ths[i]).minWidth;
                }
            }
                , () => null
                , updateHeader ? () => this.updateWidthHeader(true) : () => null
            )
        )
    }
}