import { ViolationStatus, SmartRuleSeverity, BetStatus, Bookie } from "./enums";
import { formatDate } from "@angular/common";
import { PageSearch } from "./pagination.model";
import { isUndefined } from "util";
import { QueryBuilder } from "./query-builder-config.model";
import { Alg } from "./alg";
import { AgentAccountResponse } from "./agent-account.model";

export class ViolationSearch {
    page: PageSearch;

    ruleId: string;
    userName: string;
    fullName: string;
    status: ViolationStatus;
    severity: SmartRuleSeverity;
    date: SearchDate;

    get css_severity(): string {
        return "severity-text-" + SmartRuleSeverity[this.severity];
    }

    constructor(timeZone: string) {
        this.page = new PageSearch();

        this.ruleId = "";
        this.userName = "";
        this.fullName = "";
        this.status = ViolationStatus.All;

        const now = new Date();
        this.date = new SearchDate(new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1), now, timeZone);
    }

    get url() {
        const ruleId = "/" + (this.ruleId != "" ? this.ruleId.replace(/\s+/g, '') : "");
        var params = ruleId + '?status=' + this.status;
        params += '&fromDate=' + this.date.fromDate;
        params += '&toDate=' + this.date.toDate;
        if (this.userName != "")
            params += '&userName=' + this.userName.replace(/\s+/g, '');
        if (this.fullName != "")
            params += '&fullName=' + Alg.rem2Space(this.fullName.trim());
        if (this.severity)
            params += '&severity=' + this.severity;
        params += '&pageSize=' + this.page.size;
        params += '&pageIndex=' + this.page.index;

        return params;
    }
}

export class RiskTaggingSearch {
    page: PageSearch;
    bookie: string;
    keyword: string;

    constructor() {
        this.page = new PageSearch();
        this.bookie = Bookie[Bookie.PIN];
        this.keyword = "";
    }

    get url() {
        var params = "?size=" + this.page.size;
        params += "&page=" + this.page.index;
        if (this.keyword != "")
            params += "&search=" + Alg.rem2Space(this.keyword.trim());

        return params;
    }

    get exportUrl() {
        var params = "?size=0";
        if (this.keyword != "")
            params += "&search=" + Alg.rem2Space(this.keyword.trim());

        return params;
    }
}

export class RiskTagLogSearch {
    page: PageSearch;
    bookie: string;
    keyword: string;
    date: SearchDate;

    constructor(timeZone: string) {
        this.page = new PageSearch();
        this.keyword = "";
        const now = new Date();
        this.date = new SearchDate(new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7), now, timeZone);
    }

    get url() {
        var params = "?fromDate=" + this.date.fromDate;
        params += "&toDate=" + this.date.toDate;
        if (this.keyword != "")
            params += "&search=" + Alg.rem2Space(this.keyword.trim());
        params += "&size=" + this.page.size;
        params += "&page=" + this.page.index;

        return params;
    }
}

export class BetsHistorySearch {
    page: PageSearch;
    bookie: Bookie;
    betStatus: BetStatus;

    sportsType: number;
    matchId: string;
    userNameCode: string;
    fullName: string;
    bookieRef: string;
    date: SearchDate;
    dateField: "BetDate" | "EventDate";

    _query: QueryBuilder;
    private timeZone: string;

    constructor(timeZone: string, betStatus: BetStatus, useQuery?: boolean) {
        this.page = new PageSearch();
        this.bookie = -1; //All
        this.betStatus = betStatus;

        this.sportsType = null;
        this.matchId = "";
        this.userNameCode = "";
        this.fullName = "";
        this.bookieRef = "";
        this.dateField = "BetDate";

        const now = new Date();
        this.date = new SearchDate(new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7), now, timeZone);

        if (useQuery)
            this._query = new QueryBuilder(this.dateField, this.sportsType, this.date._toDate.toISOString().substr(0, 10));
        this.timeZone = timeZone;
    }

    get query() {
        //is Settled bets || Outstanding bets
        if (!isUndefined(this._query))
            return this._query.queryWithFormatDate(this.dateField, ["BetDate", "EventDate"], this.timeZone);

        //is Bets history
        var query = new QueryBuilder(this.dateField, this.sportsType, this.date.fromDate, this.date.toDate);
        if (this.matchId != "")
            query.rules.push({ field: "MatchId", operator: "is", value: this.matchId.replace(/\s+/g, '') });
        if (this.userNameCode != "")
            query.rules.push({ field: "UserNameCode", operator: "like", value: this.userNameCode.replace(/\s+/g, '') });
        if (this.bookieRef != "")
            query.rules.push({ field: "BookieRef", operator: "like", value: this.bookieRef.replace(/\s+/g, '') });
        if (this.bookie > -1)
            query.rules.push({ field: "Bookie", operator: "is", value: this.bookie });

        return query;
    }

    get url() {
        var params = "?&pageIndex=" + this.page.index + "&pageSize=" + this.page.size
            + "&queryString=" + JSON.stringify(this.query)
            + (this.fullName != "" ? ("&fullName=" + Alg.rem2Space(this.fullName.trim())) : "")
            + "&betStatus=" + this.betStatus;

        return params;
    }

    get exportUrl() {
        var params = "?bookie=" + this.bookie
            + "&queryString=" + JSON.stringify(this.query)
            + (this.fullName != "" ? ("&fullName=" + Alg.rem2Space(this.fullName.trim())) : "")
            + "&pageSize=0" //disable paging
            + "&betStatus=" + this.betStatus;

        return params;
    }
}

export class AgentAccountSearch {
    page: PageSearch;
    model: AgentAccountResponse;

    constructor() {
        this.page = { index: 1, size: 5 };
        this.model = { domain: "", userName: "", securityCode: "", source: "All", isUpdated: null, agentStatus: null, key: "", captchaImg: null }
    }

    get Url() {
        var params = "?page=" + this.page.index + "&size=" + this.page.size

        if (this.model.domain != "")
            params += "&domain=" + this.model.domain;

        if (this.model.source != "All")
            params += "&source=" + this.model.source;

        if (this.model.userName != "")
            params += "&userName=" + this.model.userName;

        return params;
    }
}

export class SearchDate {
    _fromDate: Date;
    _toDate: Date;
    timeZone: string;

    constructor(fromDate: Date, toDate: Date, timeZone: string) {
        this._fromDate = fromDate;
        this._toDate = toDate;
        this.timeZone = timeZone;
    }

    get fromDate(): string {
        var fromDate = new Date(this._fromDate);
        if (isNaN(fromDate.getTime())) return '';

        if (this.timeZone == '+0000')
            fromDate.setHours(0, 0, 0, 0);
        else
            fromDate.setHours(0, fromDate.getTimezoneOffset(), 0, 0);

        return formatDate(fromDate, 'yyyy-MM-ddTHH:mm:ss.SSS', 'en') + 'Z';
    }

    get toDate(): string {
        var toDate = new Date(this._toDate);
        if (isNaN(toDate.getTime())) return '';

        if (this.timeZone == '+0000')
            toDate.setHours(23, 59, 59, 999);
        else
            toDate.setHours(23, 59 + toDate.getTimezoneOffset(), 59, 999);

        return formatDate(toDate, 'yyyy-MM-ddTHH:mm:ss.SSS', 'en') + 'Z';
    }
}