import { ViolationDetails, ViolationResponse, ViolationView } from "./violation.model";

import { formatDate } from "@angular/common";
import { isNullOrUndefined, isNull } from "util";
import { OddsType, Bookie } from "./enums";
import { MemberRiskTagResponse, MemberRiskTagView, MemberRiskTagDb } from "./member-risk-tag.model";
import { BetView, BetResponse } from "./bet.model";

export class ViolationDetail {
    userCode: string;
    source: number;
    data: BetView[];
    dataType: number;

    constructor(res: ViolationDetails, timeZone: string) {
        this.userCode = res.userCode;
        this.source = res.source;
        this.data = this.parseData(res.data, timeZone);
        this.dataType = res.dataType;
    }

    parseData(res: string, timeZone: string): BetView[] {
        const data: ViolationDetailDataParse[] = JSON.parse(res);

        var betView: BetView[] = [];
        for (var i = 0; i < data.length; i++) {
            var bet: any = {}; //BetResponse

            for (var key in data[i])
                bet[this.transProp(key)] = data[i][key];

            if (data[i].MemberRiskTag)
                bet.memberRiskTag = {
                    memberFullName: data[i].MemberRiskTag.MemberFullName
                    , memberPt: data[i].MemberRiskTag.MemberPt
                    , userName: data[i].MemberRiskTag.UserName
                    , positionTagValue: undefined
                    , source: undefined
                }

            if (isNullOrUndefined(bet.deltaOdds))
                bet.deltaOdds = []
            betView[i] = new BetView(bet, timeZone);
        }

        return betView;
    }

    transProp(prop: string): string {
        return prop[0].toLowerCase() + prop.substring(1, prop.length);
    }
}
export interface ViolationDetailDataParse {
    Id?: any;
    Bookie: number | string;
    UserNameCode: string;
    Sport?: any;
    Cover?: any;
    Market?: any;
    Score?: any;
    ResultHT?: any;
    ResultFT?: any;
    MatchId: number;
    League?: any;
    HomeName?: any;
    AwayName?: any;
    EventDate: string;
    SportsType: number;
    Type?: any;
    BetSelection?: any;
    BetDate: string;
    HdpOrGoal: number;
    Odds: any;
    EuroOdds: any;
    OddsType: string;
    Stake: number;
    Volume: number;
    IpAddress?: any;
    BookieRef?: any;
    BetStatus?: any;
    CountryCode?: any;
    IsSettled: boolean;
    IsCancelled: boolean;
    IsCalculated: boolean;
    LastOdds?: any;
    CurrencyCode?: any;
    Status: string;
    DetailLevelResults?: any;
    BetBag: number;
    SettlementDateTime?: any;
    BetResult?: any;
    MemberRiskTag: MemberRiskTagDb;
}

export interface ViolationDetailReponse {
    violation: ViolationResponse;
    memberPT: MemberRiskTagResponse;
}

export class ViolationDetailEdit {
    memberPT: MemberRiskTagResponse;
    get css_pt(): string {
        return "pt-text-" + this.memberPT.memberPt;
    }

    constructor(memberPT: MemberRiskTagResponse) {
        this.memberPT = memberPT;
    }
}

export class ViolationDetailView {
    memberPT: MemberRiskTagView;
    violation: ViolationView;

    constructor(mem_res: MemberRiskTagResponse, vio_res: ViolationResponse, timeZone: string) {
        this.memberPT = new MemberRiskTagView(mem_res);
        this.violation = new ViolationView(vio_res, timeZone);
    }

    static getExportData(rowsView: BetView[]) {
        var data = [];

        for (var i = 0; i < rowsView.length; i++) {
            data.push({
                col1: rowsView[i].userNameCode
                , col2: rowsView[i].memberRiskTag.memberFullName
                , col3: rowsView[i].ipAddress
                , col4: rowsView[i].countryCode.replace('(', '').replace(') ', '')
                , col5: rowsView[i].memberRiskTag.memberPt
                , col6: rowsView[i].bookie
                , col7: rowsView[i].bookieRef
                , col8: rowsView[i].hdpOrGoal
                , col9: rowsView[i].type
                , col10: rowsView[i].betSelection
                , col11: rowsView[i].cover
                , col12: rowsView[i].score
                , col13: rowsView[i].odds
                , col14: rowsView[i].betDate
                , col15: rowsView[i].matchId
                , col16: rowsView[i].league
                , col17: rowsView[i].homeName
                , col18: rowsView[i].awayName
                , col19: rowsView[i].eventDate
                , col20: rowsView[i].stake
                , col21: rowsView[i].currencyCode
                , col22: rowsView[i].odds
                , col23: rowsView[i].oddsType
                , col24: rowsView[i].euroOdds
                , col25: (rowsView[i].market == "PreLive" && !isNullOrUndefined(rowsView[i].lastOdds) && !isNullOrUndefined(rowsView[i].euroOdds) && rowsView[i].lastOdds < rowsView[i].euroOdds) ? "DROP!!!" :
                    (rowsView[i].market == "PreLive" && !isNullOrUndefined(rowsView[i].lastOdds) && isNullOrUndefined(rowsView[i].euroOdds) && rowsView[i].oddsType == "EU" && rowsView[i].lastOdds < rowsView[i].odds) ? "DROP!!!" : ""
                , col26: rowsView[i].lastOdds != "_" && rowsView[i].lastOdds != 0 ? rowsView[i].lastOdds : "N/A"    //new BetView
                , col27: rowsView[i].sport
                , col28: rowsView[i].market
                , col29: rowsView[i].status
                , col30: rowsView[i].resultFT
            })

            for (var key in data[i])
                if (isNullOrUndefined(data[i][key]))
                    data[i][key] = "";
        }

        return data;
    }
    static get exportHeader(): string[] {
        return ["Username code", "Member fullname", "Ip address", "Country code", "Member PT", "Bookie", "Bookie ref", "Hdp or goal", "Type", "Bet selection", "Cover"
            , "Score", "Odds", "Bet date", "Match id", "League", "Home name", "Away name", "Event date", "Stake", "Currency code", "Odds", "Odds type", "Euro odds", "Drop", "Last odds"
            , "Sport", "Market", "Status", "Result FT"];
    }
}