import { AgentAccountStatus, Bookie } from "./enums";
import { isNullOrUndefined } from "@swimlane/ngx-datatable/release/utils";

export class AgentAccount {

  constructor(key: string = '', source: string = '', userName: string = '') {

    this.key = key;
    this.source = source;
    this.userName = userName;
  }

  public key: string;
  public domain: string;
  public userName: string;
  public password: string;
  public securityCode: string;
  public source: string;
  public status: number;
  public agentStatus: AgentAccountStatus;

  public captchaCode: string;
  public captchaText: string;

  public resolveCaptcha: boolean;
}


//new Agent Account Struct
export interface AgentAccountResponse {
  domain: string;
  userName: string;
  securityCode: string;
  source: string;
  isUpdated: boolean;
  agentStatus: AgentAccountStatus;
  key: string;
  captchaImg: string;
}

export interface AgentStatusResponse {
  userName: string;
  captchaImg: string;
  agentStatus: AgentAccountStatus;
}

export class AgentAccountView {
  domain: string;
  userName: string;
  securityCode: string;
  source: string;
  isUpdated: boolean;
  agentStatus: AgentAccountStatus;
  key: string;
  captchaImg: string;

  constructor(res: AgentAccountResponse) {
    if (res.key != res.source + '|' + res.userName) { console.log("NOT SYNC MODEL: " + res.key); return; }
    //if (res.agentStatus == AgentAccountStatus.WaitForRun && res.source == "SBO") res.agentStatus = AgentAccountStatus.Running;

    this.domain = res.domain;
    this.userName = res.userName;
    this.securityCode = res.securityCode;
    this.source = res.source;
    this.agentStatus = res.agentStatus;
    this.captchaImg = res.captchaImg;
    this.agentStatusView = AgentAccountStatus[res.agentStatus];
    this.css_agentStatus = "agent-status-" + this.agentStatusView;
  }

  resolveText?;
  captchaText?;
  agentStatusView?: string;
  css_agentStatus?: string;

  updateStt(stt: AgentStatusResponse) {
    this.resolveText = '';
  }

  updateSttFromNode(stt: Captcha) {
    this.resolveText = '';
    this.captchaImg = stt.CaptchaImg;
    this.agentStatus = stt.AgentStatus;
    this.agentStatusView = AgentAccountStatus[stt.AgentStatus];
    this.css_agentStatus = "agent-status-" + this.agentStatusView;
  }

  static getExportData(rowsView: AgentAccountView[]) {
    var data = [];

    for (var i = 0; i < rowsView.length; i++) {
      data.push({
        col1: (i + 1)
        , col2: rowsView[i].source
        , col3: rowsView[i].domain
        , col4: rowsView[i].userName
        , col5: rowsView[i].securityCode
        , col6: rowsView[i].agentStatusView
      })
    }

    return data;
  }

  static getExportHeader(): string[] {
    var headers: string[] = [];
    headers.push("#No", "Bookie", "Domain", "Username", "Security Code", "Status");

    return headers;
  }
}

export class AgentAccountEdit {
  domain: string;
  userName: string;
  password: string;
  confirmPassword: string;
  securityCode: string;
  readonly source: string;
  readonly isUpdated: boolean;
  agentStatus: AgentAccountStatus;
  readonly key: string;

  constructor(res?: AgentAccountResponse) {
    if (isNullOrUndefined(res)) return;

    this.domain = res.domain;
    this.userName = res.userName;
    this.password = "";
    this.confirmPassword = "";
    this.securityCode = res.securityCode;
    this.source = res.source;
    this.isUpdated = res.isUpdated;
    this.agentStatus = res.agentStatus;
    this.key = res.key;
  }
}

export class Captcha {
  Source: string;
  CaptchaImg: string;
  UserName: string;
  AgentStatus: AgentAccountStatus;
}