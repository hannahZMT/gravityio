import { AuditValue } from "./audit-value-model";
import { formatDate } from "@angular/common";

export interface SmartRulesHistoryResponse {
    updatedBy: string;
    updatedDate: Date;
    auditValues: AuditValue[];
}

export class SmartRulesHistoryView {
    readonly updatedBy: string;
    readonly updatedDate: string;
    readonly auditValues: string[];

    constructor(obj: SmartRulesHistoryResponse, timeZone: string) {
        this.updatedBy = obj.updatedBy;
        this.updatedDate = formatDate(obj.updatedDate, 'dd/MM/yyy HH:mm:ss', 'en', timeZone);
        this.auditValues = AuditValue.parseAudit(obj.auditValues);
    }
}