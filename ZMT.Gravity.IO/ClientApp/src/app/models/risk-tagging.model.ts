import { RiskTaggingStatus } from "./enums";
import { PositionTag } from "./position-tag.model";
import { formatDate } from "@angular/common";
import { RiskTaggingField } from "./risk-tagging-field.model";
import { isNullOrUndefined } from "util";

export class RiskTaggingEdit {
    id: string;
    fullName: string;
    currency: string;
    country: string;
    kyc: string;
    bonus: string;
    dateOfBirth: Date;
    dateOfStart: Date;
    positionTagValue: number;
    status: RiskTaggingStatus;
    firstDepEurBtc: string;
    firstDepGbp: string;
    pinBet: string;
    maxBet: string;
    sboBet: string;
    skypeBet: string;
    singBet: string;
    betIsn: string;
    matchBook: string;
    aoEu: string;
    aoGb: string;
    ga288: string;
    nightWickets: string;
    orbEu: string;
    isDeleted: boolean;

    get css_status(): string {
        return "status-" + RiskTaggingStatus[this.status];
    }

    constructor(res?: RiskTaggingResponse, timeZone?: string) {
        if (!res) return;
        this.id = res.id;

        this.fullName = res.fullName;
        this.currency = res.currency;
        this.country = res.country;
        this.dateOfBirth = res.dateOfBirth ? new Date(res.dateOfBirth) : undefined;
        this.dateOfStart = res.dateOfStart ? new Date(res.dateOfStart) : undefined;
        this.positionTagValue = res.positionTagValue;
        this.status = res.status;
        this.kyc = res.kyc;
        this.bonus = res.bonus;
        this.firstDepEurBtc = res.firstDepEurBtc;
        this.firstDepGbp = res.firstDepGbp;
        this.pinBet = res.pinBet;
        this.maxBet = res.maxBet;
        this.sboBet = res.sboBet;
        this.skypeBet = res.skypeBet;
        this.singBet = res.singBet;
        this.betIsn = res.betIsn;
        this.matchBook = res.matchBook;
        this.aoEu = res.aoEu;
        this.aoGb = res.aoGb;
        this.ga288 = res.ga288;
        this.nightWickets = res.nightWickets;
        this.orbEu = res.orbEu;
    }
}


export interface RiskTaggingResponse {
    id: string;
    fullName: string;
    currency: string;
    country: string;
    kyc: string;
    bonus: string;
    dateOfBirth?: Date;
    dateOfStart?: Date;
    positionTagValue: number;
    status: number;
    firstDepEurBtc: string;
    firstDepGbp: string;
    pinBet: string;
    maxBet: string;
    sboBet: string;
    skypeBet: string;
    singBet: string;
    betIsn: string;
    matchBook: string;
    aoEu: string;
    aoGb: string;
    ga288: string;
    nightWickets: string;
    orbEu: string;
    isDeleted: boolean;
    userLastChanged: string;
    dateLastChanged: Date;
    histories?: any;
}

export class RiskTaggingView {
    id: string;
    fullName: string;
    currency: string;
    country: string;
    kyc: string;
    bonus: string;
    dateOfBirth: string;
    dateOfStart: string;
    positionTagName: string;
    status: string
    firstDepEurBtc: string;
    firstDepGbp: string;
    pinBet: string;
    maxBet: string;
    sboBet: string;
    skypeBet: string;
    singBet: string;
    betIsn: string;
    matchBook: string;
    aoEu: string;
    aoGb: string;
    ga288: string;
    nightWickets: string;
    orbEu: string;
    isDeleted: boolean;
    userNameBookies: UserNameBookie[];

    css_status: string;
    css_memberPt: string;

    constructor(res: RiskTaggingResponse, pts: PositionTag[], timeZone: string) {
        this.id = res.id;

        this.fullName = res.fullName;
        this.currency = res.currency;
        this.country = res.country;
        this.kyc = res.kyc;
        this.bonus = res.bonus;
        this.dateOfBirth = res.dateOfBirth ? formatDate(res.dateOfBirth, "dd/MM/yyyy", "en", timeZone) : undefined;
        this.dateOfStart = res.dateOfStart ? formatDate(res.dateOfStart, "dd/MM/yyyy", "en", timeZone) : undefined;
        this.positionTagName = RiskTaggingView.getPositionTagName(res.positionTagValue, pts);
        this.status = RiskTaggingStatus[res.status];
        this.firstDepEurBtc = res.firstDepEurBtc;
        this.firstDepGbp = res.firstDepGbp;
        //this.isDeleted = res.isDeleted;

        this.pinBet = res.pinBet;// ? res.pinBet : "";
        this.maxBet = res.maxBet;// ? res.maxBet : "";
        this.sboBet = res.sboBet;// ? res.sboBet : "";
        this.skypeBet = res.skypeBet;// ? res.skypeBet : "";
        this.singBet = res.singBet;// ? res.singBet : "";
        this.betIsn = res.betIsn;// ? res.betIsn : "";
        this.matchBook = res.matchBook;// ? res.matchBook : "";
        this.aoEu = res.aoEu;// ? res.aoEu : "";
        this.aoGb = res.aoGb;// ? res.aoGb : "";
        this.ga288 = res.ga288;// ? res.ga288 : "";
        this.nightWickets = res.nightWickets;// ? res.nightWickets : "";
        this.orbEu = res.orbEu;// ? res.orbEu : "";

        this.userNameBookies = this.getUserNameBookies(res);

        this.css_status = "status-" + this.status;
        this.css_memberPt = "pt-div pt-div-" + this.positionTagName.replace(/\s+/g, '');
    }

    getUserNameBookies(res: RiskTaggingResponse): UserNameBookie[] {
        var tmp: UserNameBookie[] = [];

        if (res.pinBet) tmp.push(new UserNameBookie("PINBET", res.pinBet));
        if (res.maxBet) tmp.push(new UserNameBookie("MAXBET", res.maxBet));
        if (res.sboBet) tmp.push(new UserNameBookie("SBOBET", res.sboBet));
        if (res.skypeBet) tmp.push(new UserNameBookie("SKYPE BETTING", res.skypeBet));
        if (res.singBet) tmp.push(new UserNameBookie("SINGBET", res.singBet));
        if (res.betIsn) tmp.push(new UserNameBookie("BETISN", res.betIsn));
        if (res.matchBook) tmp.push(new UserNameBookie("MATCHBOOK", res.matchBook));
        if (res.aoEu) tmp.push(new UserNameBookie("AO(EU)", res.aoEu));
        if (res.aoGb) tmp.push(new UserNameBookie("AO(GB)", res.aoGb));
        if (res.ga288) tmp.push(new UserNameBookie("GA288", res.ga288));
        if (res.nightWickets) tmp.push(new UserNameBookie("9WICKETS", res.nightWickets));
        if (res.orbEu) tmp.push(new UserNameBookie("Orb(EU)", res.orbEu));

        return tmp;
    }

    static getPositionTagName(value: number, pts: PositionTag[]): string {
        for (var i = 0; i < pts.length; i++)
            if (pts[i].value == value)
                return pts[i].name;
        return undefined;
    }

    static getExportData(rowsView: RiskTaggingView[]) {
        var data = [];

        const props = RiskTaggingField.props;
        for (var i = 0; i < rowsView.length; i++) {
            var obj = {};
            for (var j = 0; j < props.length; j++)
                obj["col" + (j + 1)] = rowsView[i][props[j]]
            data.push(obj)

            for (var key in data[i])
                if (isNullOrUndefined(data[i][key]))
                    data[i][key] = "";
        }

        return data;
    }
    static get exportHeader(): string[] {
        return RiskTaggingField.names;
    }
}

export class UserNameBookie {
    bookieName: string;
    userName: string;

    constructor(bookieName: string, userName: string) {
        this.bookieName = bookieName;
        this.userName = userName;
    }

    toString(): string {
        return this.bookieName + " :" + this.userName;
    }
}