import { AuditValue } from "./audit-value-model";
import { formatDate } from "@angular/common";


export interface RiskTagLogResponse {
  fullName: string;
  action: string;
  updatedBy: string;
  updatedDate: Date;
  auditValues: AuditValue[];
}

export class RiskTagLogView {
  readonly fullName: string;
  readonly action: string;
  readonly updatedBy: string;
  readonly updatedDate: string;
  readonly auditValues: string[];

  constructor(res: RiskTagLogResponse, timeZone: string) {
    this.fullName = res.fullName;
    this.action = res.action;
    this.updatedBy = res.updatedBy;
    this.updatedDate = formatDate(res.updatedDate, 'dd/MM/yyy HH:mm:ss', 'en', timeZone);
    this.auditValues = AuditValue.parseAudit(res.auditValues);
  }
}


