import { LevelResult } from "./levelresult.model";
import { OddsType, BetStatus, Bookie } from "./enums";
import { MemberRiskTagResponse, MemberRiskTagView } from "./member-risk-tag.model";
import { formatDate } from "@angular/common";
import { isNullOrUndefined, isNull } from "util";
import { faBan, faChevronDown, faAngleDoubleDown, faArrowRight, faChevronUp, faAngleDoubleUp, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { BetsHistorySearch } from "./search.model";

export interface BetResponse {
  matchId: number;
  league: string;
  homeName: string;
  awayName: string;
  userNameCode: string;
  type: string;
  betSelection: string;
  cover: string;
  market: string;
  hdpOrGoal: number;
  bookieRef: string;
  ipAddress: string;
  stake: number;
  eventDate: string;
  betDate: string;
  odds: number;
  euroOdds: number;
  lastOdds: number;
  deltaOdds: number[];
  score: string;
  sport: string;
  status: string;
  currencyCode: string;
  resultFT: string;
  resultHT: string;
  volume: number;
  detailLevelResults: LevelResult[];

  id: string;
  bookie: number;
  sportsType: number;
  oddsType: OddsType;
  betStatus: string;
  countryCode: string;
  isSettled: boolean;
  isCancelled: boolean;
  isCalculated: boolean;
  betBag: BetStatus;

  memberRiskTag: MemberRiskTagResponse;
}

export class BetView {
  matchId: string;
  league: string;
  homeName: string;
  awayName: string;
  userNameCode: string;
  type: string;
  betSelection: string;
  cover: string;
  market: string;
  hdpOrGoal: number;
  bookieRef: string;
  ipAddress: string;
  stake: number;
  eventDate: string;
  betDate: string;
  odds: string;
  euroOdds: string;
  lastOdds: string | number;
  deltaOdds: string[] | number[];
  score: string;
  sport: string;
  status: string;
  currencyCode: string;
  resultFT: string;
  resultHT: string;
  volume: number;
  detailLevelResults: LevelResult[];

  id: string;
  bookie: string;
  sportsType: number;
  oddsType: string;
  betStatus: string;
  countryCode: string;
  isSettled: boolean;
  isCancelled: boolean;
  isCalculated: boolean;
  betBag: BetStatus;

  memberRiskTag: MemberRiskTagView;

  winLossAmount: string | number = "_";
  agentPt: string;
  agentAmount: string | number = "_";
  vs: string;

  icon: OddIcon;

  constructor(obj: BetResponse, timeZone: string) {
    for (var key in obj)
      this[key] = obj[key];

    this.memberRiskTag = new MemberRiskTagView(obj.memberRiskTag);
    this.countryCode = isNullOrUndefined(obj.countryCode) ? "" : '(' + obj.countryCode + ') ';

    this.matchId = !isNullOrUndefined(obj.matchId) ? obj.matchId.toString() : "";

    this.betDate = formatDate(obj.betDate, 'dd/MM/yyy HH:mm:ss', 'en', timeZone);
    this.eventDate = formatDate(obj.eventDate, 'dd/MM/yyy HH:mm:ss', 'en', timeZone);
    this.score = isNullOrUndefined(obj.score) ? "" : this.score + " ";

    this.odds = (!isNullOrUndefined(obj.odds) && obj.oddsType == OddsType.AM
      ? obj.odds
      : (!isNullOrUndefined(obj.odds) && obj.oddsType != OddsType.AM
        ? Math.round(obj.odds * 1000) / 1000
        : '')).toString();
    this.euroOdds = obj.euroOdds ? (Math.round(obj.euroOdds * 1000) / 1000).toString() : '';
    this.oddsType = isNullOrUndefined(obj.oddsType) ? "" : OddsType[obj.oddsType];
    this.lastOdds = isNullOrUndefined(obj.lastOdds) ? "_" : obj.lastOdds;

    this.deltaOdds[0] = isNullOrUndefined(obj.deltaOdds) || isNullOrUndefined(obj.deltaOdds[0]) ? "_" : obj.deltaOdds[0];
    this.deltaOdds[1] = isNullOrUndefined(obj.deltaOdds) || isNullOrUndefined(obj.deltaOdds[1]) ? "_" : obj.deltaOdds[1];
    this.deltaOdds[2] = isNullOrUndefined(obj.deltaOdds) || isNullOrUndefined(obj.deltaOdds[2]) ? "_" : obj.deltaOdds[2];
    this.icon = new OddIcon(obj.lastOdds, obj.deltaOdds[0], obj.deltaOdds[1], obj.deltaOdds[2], obj.euroOdds);

    this.vs = isNullOrUndefined(obj.awayName) ? "" : " vs ";
    this.bookie = isNullOrUndefined(obj.bookie) ? "" : Bookie[obj.bookie];

    //settled bets
    const n = this.detailLevelResults.length - 1;
    if (n > 0 && obj.betBag != BetStatus.OPEN) {
      this.winLossAmount = this.detailLevelResults[n].ptResult;
      this.agentPt = this.detailLevelResults[n - 1].ptPercent * 100 + "%";
      this.agentAmount = this.detailLevelResults[n - 1].ptResult;
    }
  }

  static getExportData(betStatus: BetStatus, rowsView: BetView[], searchParams: BetsHistorySearch) {
    var data = [];

    for (var i = 0; i < rowsView.length; i++)
      data.push({
        col1: rowsView[i].userNameCode + '\r\n'
          + rowsView[i].memberRiskTag.memberFullName + '\r\n'
          + rowsView[i].countryCode + rowsView[i].ipAddress + '\r\n'
          + rowsView[i].memberRiskTag.memberPt
        , col2: searchParams.bookie + ' ' + rowsView[i].bookieRef + '\r\n'
          + rowsView[i].hdpOrGoal + rowsView[i].type + '@' + rowsView[i].betSelection + '\r\n'
          + rowsView[i].cover + rowsView[i].score + '@' + rowsView[i].odds + '\r\n'
          + rowsView[i].betDate
        , col3: rowsView[i].matchId + ' ' + rowsView[i].league + '\r\n'
          + rowsView[i].homeName + rowsView[i].vs + rowsView[i].awayName + '\r\n'
          + rowsView[i].eventDate
        , col4: rowsView[i].stake + ' ' + rowsView[i].currencyCode
        , col5: rowsView[i].odds + rowsView[i].oddsType
          + (rowsView[i].oddsType != 'EU' ? ('\r\n' + rowsView[i].euroOdds) : '')
          + ((rowsView[i].market == 'PreLive' && rowsView[i].lastOdds != null && rowsView[i].euroOdds && rowsView[i].lastOdds < Number.parseFloat(rowsView[i].euroOdds)) ? "\r\nDROP!!!" : '')
          + ((rowsView[i].market == 'PreLive' && rowsView[i].lastOdds != null && !rowsView[i].euroOdds && rowsView[i].oddsType == 'EU' && rowsView[i].lastOdds < Number.parseFloat(rowsView[i].odds)) ? "\r\nDROP!!!" : '')
        , col6: rowsView[i].lastOdds != '_' && rowsView[i].lastOdds != 0 ? rowsView[i].lastOdds : "N/A"
        , col7: rowsView[i].deltaOdds[0] != null && rowsView[i].deltaOdds[0] != 0 ? rowsView[i].deltaOdds[0] : "N/A"
        , col8: rowsView[i].deltaOdds[1] != null && rowsView[i].deltaOdds[1] != 0 ? rowsView[i].deltaOdds[1] : "N/A"
        , col9: rowsView[i].deltaOdds[2] != null && rowsView[i].deltaOdds[2] != 0 ? rowsView[i].deltaOdds[2] : "N/A"
        , col10: rowsView[i].market + '\r\n'
          + rowsView[i].sport
        , col11: rowsView[i].status + '\r\n'
          + rowsView[i].resultFT
      })

    if (betStatus != BetStatus.OPEN)
      for (var i = 0; i < data.length; i++)
        if (rowsView[i].betBag != BetStatus.OPEN) {
          data[i]["col12"] = rowsView[i].winLossAmount;
          data[i]["col13"] = rowsView[i].agentAmount;
          data[i]["col14"] = rowsView[i].agentPt
        }
        else {
          data[i]["col12"] = "N/A";
          data[i]["col13"] = "N/A";
          data[i]["col14"] = "N/A";
        }

    return data;
  }
  static getExportHeader(betStatus: BetStatus): string[] {
    var headers: string[] = [];

    headers.push("Source/Member", "Bet Details", "Match Details", "Stake", "Placed Odds", "Last Odds", "D1 (1m)", "D2 (6m)", "D3 (11m)", "Market", "Match Status");

    if (betStatus != BetStatus.OPEN)
      headers.push("Member PnL", "Agent Amount", "Agent PT%")

    return headers;
  }
}

export class OddIcon {
  lastOdds: IconStyle;
  d1: IconStyle;
  d2: IconStyle;
  d3: IconStyle;

  constructor(lastOdds: number, d1: number, d2: number, d3: number, euroOdds: number) {
    this.lastOdds = new IconStyle(lastOdds, euroOdds);
    this.d1 = new IconStyle(d1, euroOdds);
    this.d2 = new IconStyle(d2, euroOdds);
    this.d3 = new IconStyle(d3, euroOdds);
  }
}

export class IconStyle {
  icon: IconDefinition;
  style: 'red' | 'green' | 'gray' | 'transparent';

  constructor(oddField: number, oddField2: number) {
    if (!isNullOrUndefined(oddField) && !isNullOrUndefined(oddField2)) {
      if (oddField == 0) {
        this.icon = faBan;
        this.style = 'red';
        return;
      }

      if (oddField == oddField2) {
        this.icon = faArrowRight;
        this.style = 'gray';
        return;
      }

      if (oddField * 1.5 < oddField2) {
        this.icon = faAngleDoubleDown;
        this.style = 'red';
        return;
      }

      if (oddField < oddField2) {
        this.icon = faChevronDown;
        this.style = 'red';
        return;
      }

      if (oddField > oddField2 * 1.5) {
        this.icon = faAngleDoubleUp;
        this.style = 'green';
        return;
      }

      if (oddField > oddField2) {
        this.icon = faChevronUp;
        this.style = 'green';
        return;
      }

      this.icon = faBan;
      this.style = 'transparent';
    }
  }
}