import { Alg } from "./alg";

//nav
export enum BetStatus {
  OPEN = 0,
  SETTLED = 1,
  ALL = 4,
}

export enum BetSelection {
  HOME = 1,
  AWAY = 2,
}

export enum OddsType {
  //[American, Decimal, HongKong, Indonesian, Malay]
  Unknown = -1,
  HK = 0,
  EU = 1,
  MY = 2,
  AM = 3,
  ID = 4,
}

export enum RiskTaggingStatus {
  Active = 0,
  Inactive = 1,
  Suspended = 2
}

export enum AgentAccountStatus {
  WaitForRun = 0,
  Running = 1,
  Pause = 2,
  Stop = 3,
  WaitForStop = 4,
}

export enum SmartRuleSeverity {
  //Information = 0,//Green - Low
  Low = 1,//Green
  Normal = 2,//Orange
  High = 3,//Red
  //Error = 4//Red - Severe
}

export enum ViolationStatus {
  All = -1,
  Pending = 0,
  Reviewed = 1,
}

//search
export enum Bookie {
  GA = 0,
  IBC = 1,
  ISN = 2,
  PIN = 3,
  SBO = 4,
  SIN = 5,
  MAX = 6,
  MatchBook = 7,
  NightWickets = 8
}

export enum Sport {
  Unknown = -1,
  Soccer = 0,
  Badminton = 1,
  Bandy = 2,
  Baseball = 3,
  Basketball = 4,
  BeachVolleyball = 5, //Beach Volleyball
  Boxing = 6,
  Chess = 7,
  Cricket = 8,
  Curling = 9,
  Darts = 10,
  DartsLegs = 11,//Darts (Legs)
  ESports = 12,
  Football = 13,
  FieldHockey = 14,//Field Hockey
  Floorball = 15,
  Futsal = 16,
  Tennis = 17,
  Golf = 18,
  Handball = 19,
  Hockey = 20,
  HorseRacing = 21,//Horse Racing
  Volleyball = 22,
  MixedMartialArts = 23,//Mixed Martial Arts
  RugbyLeague = 24,//Rugby League
  Politics = 25,
  RugbyUnion = 26,//Rugby Union
  Snooker = 27,
  Softball = 28,
  Squash = 29,
  TableTennis = 30,//Table Tennis
  VolleyballPoints = 31,//Volleyball (Points)
  WaterPolo = 32,//Water Polo
  AussieRules = 33,//Aussie Rules
  AlpineSkiing = 34,//AlpineSkiing
  Biathlon = 35,
  SkiJumping = 36,//Ski Jumping
  CrossCountry = 37,//Cross Country
  Formula1 = 38,//Formula 1
  Cycling = 39,
  Bobsleigh = 40,
  FigureSkating = 41,//Figure Skating
  FreestyleSkiing = 42,//Freestyle Skiing
  Luge = 43,
  NordicCombined = 44, //Nordic Combined
  ShortTrack = 45,//Short Track
  Skeleton = 46,
  SnowBoarding = 47, //Snow Boarding
  SpeedSkating = 48,//Speed Skating
  Olympics = 49,
  Athletics = 50,
  Crossfit = 51,
  Entertainment = 52,
  DroneRacing = 53,//Drone Racing
  Poker = 54,
  Motorsport = 55,
  Outright = 56,
  VirtualSports = 57
}

//other
export class EnumObj {
  value: any;
  name: any;
  css?: string;
}

export class EnumArray {
  static get RiskTaggingStatus(): EnumObj[] {
    var res: EnumObj[] = [];
    for (var i = 0; i <= 2; i++)
      res.push({ value: i, name: RiskTaggingStatus[i], css: "status-" + RiskTaggingStatus[i] });

    return res;
  }

  static get SmartRuleSeverity(): EnumObj[] {
    var res: EnumObj[] = [];
    for (var i = 1; i <= 3; i++)
      res.push({ value: i, name: SmartRuleSeverity[i], css: "severity severity-text-" + SmartRuleSeverity[i] });

    return res;
  }

  static get ViolationStatus(): EnumObj[] {
    var res: EnumObj[] = [];
    for (var i = 0; i <= 1; i++)
      res.push({ value: i, name: ViolationStatus[i] });

    return res;
  }

  static get Bookie(): EnumObj[] {
    var res: EnumObj[] = [];
    for (var i = 3; i <= 4; i++) //full : 0 -> 8
      res.push({ value: i, name: Bookie[i] });

    return res;
  }

  static get Sport(): EnumObj[] {
    var res: EnumObj[] = [];
    for (var i = -1; i <= 57; i++)
      res.push({ value: i, name: Alg.transEnum(Sport[i]) });

    return res;
  }
}