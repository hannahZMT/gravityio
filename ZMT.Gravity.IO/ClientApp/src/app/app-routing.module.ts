import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';

import { RiskTaggingComponent } from './components/risk-info/risk-tagging/risk-tagging.component';
import { RiskTagLogsComponent } from './components/risk-info/risk-tag-logs/risk-tag-logs.component';
import { BetsHistoryComponent } from './components/bets-info/bets-history/bets-history.component';
import { RealTimeBetComponent } from './components/realtime-bets/realtime-bets.component';
import { OutStandingBetsComponent } from './components/bets-info/outstanding-bets/outstanding-bets.component';
import { SettledBetComponent } from './components/bets-info/settled-bets/settled-bets.component';
import { SmartRuleComponent } from './components/smart-rule/smart-rule.component';
import { ViolationsComponent } from './components/violations/violations.component';
import { LoginComponent } from './components/login/login.component';
import { SettingsComponent } from './components/settings/settings.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AgentAccountNComponent } from './components/agent-account-n/agent-account-n.component';

const routes: Routes = [
  { path: "", redirectTo: "/real-time-bets", pathMatch: "full" },
  { path: "real-time-bets", component: RealTimeBetComponent, data: { title: "Real-time Bets" }, canActivate: [AuthGuard] },

  { path: "bets-history", component: BetsHistoryComponent, data: { title: "Bets History" }, canActivate: [AuthGuard] },
  { path: "outstanding-bets", component: OutStandingBetsComponent, data: { title: "Outstanding Bets" }, canActivate: [AuthGuard] },
  { path: "settled-bets", component: SettledBetComponent, data: { title: "Settled Bets" }, canActivate: [AuthGuard] },

  { path: "risk-tagging", component: RiskTaggingComponent, data: { title: "Risk Tagging" }, canActivate: [AuthGuard] },
  { path: "risk-tag-logs", component: RiskTagLogsComponent, data: { title: "Risk Tag Logs" }, canActivate: [AuthGuard] },

  { path: "agent-accounts", component: AgentAccountNComponent, data: { title: "Agent Accounts" }, canActivate: [AuthGuard] },  
  { path: "smart-rules", component: SmartRuleComponent, data: { title: "Smart Rules" }, canActivate: [AuthGuard] },
  { path: "violations", component: ViolationsComponent, data: { title: "Violations" }, canActivate: [AuthGuard] },
  
  { path: "login", component: LoginComponent, data: { title: "Login" } },
  { path: "settings", component: SettingsComponent, data: { title: "Settings" }, canActivate: [AuthGuard] },
  { path: "**", component: NotFoundComponent, data: { title: "Page Not Found" } }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthService, AuthGuard]
})
export class AppRoutingModule { }
