import { NgModule, ErrorHandler } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CustomFormsModule } from '@floydspace/ngx-validation';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ToastaModule } from 'ngx-toasta';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { PopoverModule } from "ngx-bootstrap/popover";
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ChartsModule } from 'ng2-charts';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { QueryBuilderModule } from 'angular2-query-builder';

import { AppRoutingModule } from './app-routing.module';
import { AppErrorHandler } from './app-error.handler';
import { AppTitleService } from './services/app-title.service';
import { AppTranslationService, TranslateLanguageLoader } from './services/app-translation.service';
import { ConfigurationService } from './services/configuration.service';
import { AlertService } from './services/alert.service';
import { LocalStoreManager } from './services/local-store-manager.service';
import { EndpointFactory } from './services/endpoint-factory.service';
import { NotificationService } from './services/notification.service';
import { NotificationEndpoint } from './services/notification-endpoint.service';
import { AccountService } from './services/account.service';
import { BetsHistoryService } from './services/bets-history.service';

import { BetsHistoryEndpoint } from './services/bets-history-endpoint.service';
import { AccountEndpoint } from './services/account-endpoint.service';
import { BetsService } from './services/bets.service';
import { BetsEndpoint } from './services/bets-endpoint.service';

import { EqualValidator } from './directives/equal-validator.directive';
import { LastElementDirective } from './directives/last-element.directive';
import { AutofocusDirective } from './directives/autofocus.directive';
import { BootstrapTabDirective } from './directives/bootstrap-tab.directive';
import { BootstrapToggleDirective } from './directives/bootstrap-toggle.directive';
import { BootstrapSelectDirective } from './directives/bootstrap-select.directive';
import { BootstrapDatepickerDirective } from './directives/bootstrap-datepicker.directive';
import { GroupByPipe } from './pipes/group-by.pipe';

import { AppComponent } from "./components/app.component";
import { LoginComponent } from "./components/login/login.component";
import { SettingsComponent } from "./components/settings/settings.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { NotificationsViewerComponent } from "./components/controls/notifications-viewer.component";
import { SearchBoxComponent } from "./components/controls/search-box.component";
import { UserInfoComponent } from "./components/controls/user-info.component";
import { UserPreferencesComponent } from "./components/controls/user-preferences.component";
import { UsersManagementComponent } from "./components/controls/users-management.component";
import { RolesManagementComponent } from "./components/controls/roles-management.component";
import { RoleEditorComponent } from "./components/controls/role-editor.component";
import { BetsHistoryComponent } from './components/bets-info/bets-history/bets-history.component';
import { OutStandingBetsComponent } from './components/bets-info/outstanding-bets/outstanding-bets.component';
import { SettledBetComponent } from "./components/bets-info/settled-bets/settled-bets.component";
import { RealTimeBetComponent } from "./components/realtime-bets/realtime-bets.component";
import { ToastyModule } from 'ng2-toasty';

import { RiskTaggingService } from "./services/risk-tagging.service";
import { RiskTaggingEndpoint } from "./services/risk-tagging-endpoint.service";
import { CountryEndpoint } from "./services/country-endpoint.service";
import { CountryService } from "./services/country.service";
import { PositionTagEndpoint } from "./services/position-tag-endpoint.service";
import { PositionTagService } from "./services/position-tag.service";

import { RiskTagLogsService } from "./services/risk-tag-logs.service";
import { RiskTagLogsEndpoint } from "./services/risk-tag-logs-endpoint.service";
import { ConfigurationEndpoint } from "./services/configuration-endpoint.service";
import { LoadingComponent } from "./components/loading/loading.component";
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { SmartRuleComponent } from "./components/smart-rule/smart-rule.component";
import { SmartRuleService } from "./services/smart-rule.service";
import { SmartRuleEndpoint } from "./services/smart-rule-endpoint.service";
import { SmartRulesHistoryComponent } from "./components/smart-rules-history/smart-rules-history.component";
import { ViolationsComponent } from "./components/violations/violations.component";
import { ViolationsEndpoint } from "./services/violations-endpoint.service";
import { ViolationsService } from "./services/violations.service";
import { ViolationDetailComponent } from "./components/violation-detail/violation-detail.component";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TagInputModule } from 'ngx-chips';
import { RiskTaggingComponent } from "./components/risk-info/risk-tagging/risk-tagging.component";
import { RiskTagLogsComponent } from "./components/risk-info/risk-tag-logs/risk-tag-logs.component";
import { IPsFilterPipe } from "./pipes/ips-filter.pipe";
import { AgentAccountNComponent } from "./components/agent-account-n/agent-account-n.component";
import { AgentAccountNService } from "./services/agent-account-n.service";
import { AgentAccountNEndpoint } from "./services/agent-account-n-endpoint.service";

@NgModule({
  imports: [
    FontAwesomeModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    CustomFormsModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: TranslateLanguageLoader
      }
    }),
    NgxDatatableModule,
    ToastaModule.forRoot(),
    ToastyModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    PopoverModule.forRoot(),
    BsDropdownModule.forRoot(),
    CarouselModule.forRoot(),
    ModalModule.forRoot(),
    QueryBuilderModule,
    ChartsModule,
    PaginationModule.forRoot(),
    TagInputModule,
    ReactiveFormsModule
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    SettingsComponent,
    UsersManagementComponent, UserInfoComponent, UserPreferencesComponent,
    RolesManagementComponent, RoleEditorComponent,
    NotFoundComponent,
    NotificationsViewerComponent,
    SearchBoxComponent,
    EqualValidator,
    LastElementDirective,
    AutofocusDirective,
    BootstrapTabDirective,
    BootstrapToggleDirective,
    BootstrapSelectDirective,
    BootstrapDatepickerDirective,
    GroupByPipe,
    BetsHistoryComponent,
    AgentAccountNComponent,
    OutStandingBetsComponent,
    SettledBetComponent,
    RealTimeBetComponent,
    RiskTaggingComponent,
    RiskTagLogsComponent,
    LoadingComponent,
    SmartRuleComponent,
    SmartRulesHistoryComponent,
    ViolationsComponent,
    ViolationDetailComponent,
    IPsFilterPipe
  ],
  providers: [
    { provide: 'BASE_URL', useFactory: getBaseUrl },
    { provide: ErrorHandler, useClass: AppErrorHandler },
    AlertService,
    ConfigurationService, ConfigurationEndpoint,
    AppTitleService,
    AppTranslationService,
    NotificationService,
    NotificationEndpoint,
    AccountService,
    AccountEndpoint,
    LocalStoreManager,
    EndpointFactory,
    BetsEndpoint,
    BetsHistoryEndpoint,
    BetsService,
    BetsHistoryService,
    AgentAccountNEndpoint,
    AgentAccountNService,
    BetsEndpoint,
    BetsService,
    RiskTaggingEndpoint,
    RiskTaggingService,
    PositionTagEndpoint,
    PositionTagService,
    RiskTagLogsEndpoint,
    RiskTagLogsService,
    CountryEndpoint,
    CountryService,
    SmartRuleService,
    SmartRuleEndpoint,
    ViolationsEndpoint, ViolationsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}


export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}
