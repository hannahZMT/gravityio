import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from "@angular/core";
import { fadeInOut } from "src/app/services/animations";
import { RiskTaggingResponse, RiskTaggingView, RiskTaggingEdit } from "src/app/models/risk-tagging.model";
import { RiskTaggingSearch } from "src/app/models/search.model";
import { UIState } from "src/app/models/ui-state.model";
import { PaginationView } from "src/app/models/pagination.model";
import { FixedHeader } from "src/app/models/fixed-header";
import { RiskTaggingService } from "src/app/services/risk-tagging.service";
import { PositionTagService } from "src/app/services/position-tag.service";
import { CountryService } from "src/app/services/country.service";
import { ConfigurationService } from "src/app/services/configuration.service";
import { AlertService, MessageSeverity, DialogType } from "src/app/services/alert.service";
import { PositionTag } from "src/app/models/position-tag.model";
import { ApiResponse, Pagging } from "src/app/models/response.model";
import { Country } from "src/app/models/country.model";
import { RiskTaggingField } from "src/app/models/risk-tagging-field.model";
import { RiskTaggingStatus, EnumArray } from "src/app/models/enums";
import { isNullOrUndefined } from "util";
import { timer, forkJoin } from "rxjs";
import 'rxjs/add/operator/map';
import { HttpEventType } from "@angular/common/http";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Angular5Csv } from "angular5-csv/Angular5-csv";
import { Alg } from "src/app/models/alg";


@Component({
  selector: 'app-risk-tagging',
  templateUrl: './risk-tagging.component.html',
  styleUrls: ['./risk-tagging.component.css'],
  animations: [fadeInOut]
})

export class RiskTaggingComponent implements OnInit, OnDestroy, AfterViewInit {

  rows: RiskTaggingResponse[];
  rowsView: RiskTaggingView[];
  searchParams: RiskTaggingSearch = new RiskTaggingSearch();
  is: UIState = { init: true, loading: true, data: false, import: false, exportCur: false, exportAll: false }
  pagin: PaginationView = new PaginationView();

  fixedHeader: FixedHeader = new FixedHeader('.risk-tagging');

  statuses = EnumArray.RiskTaggingStatus;

  constructor(
    private riskTaggingService: RiskTaggingService
    , private positionTagService: PositionTagService
    , private countryService: CountryService
    , private configService: ConfigurationService
    , private alertService: AlertService
  ) { }

  ngOnInit() {
    forkJoin([
      this.getPTs(), this.getFieldList(), this.getRiskTag()
      , this.getCountries()
    ]).subscribe(
      () => this.is.init = false
    );
  }

  ngAfterViewInit() {
    this.fixedHeader.addEventListener();
  }

  positionTags: PositionTag[];
  newPositionTag: PositionTag = new PositionTag(undefined, undefined);
  getPTs() {
    return this.positionTagService.getAll().map(
      (res: ApiResponse<PositionTag[]>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage("Error when load all PT", res.codeMessage.message, MessageSeverity.error);

        this.positionTags = res.data;
      }
    )
  }
  countries: Country[];
  getCountries() {
    return this.countryService.getCountries().map(
      (res: ApiResponse<Country[]>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage("Error when get all countries", res.codeMessage.message, MessageSeverity.error);

        this.countries = res.data;
      });
  }
  fields: RiskTaggingField[];
  getFieldList() {
    this.fields = [];

    return this.riskTaggingService.getFieldList().map(
      (res: ApiResponse<any>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage("Error when load field list", res.codeMessage.message, MessageSeverity.error);

        for (var key in res.data) {
          var field = new RiskTaggingField(key, res.data[key]);
          this.fields.push(field);
        }

        timer(1).subscribe(() => this.fixedHeader.init());
      }
    )
  }

  getRiskTag() {
    this.is.data = false;
    this.is.loading = true;

    return this.riskTaggingService.getRiskTaggings(this.searchParams).map(
      (res: ApiResponse<Pagging<RiskTaggingResponse[]>>) => {

        if (!this.error(res)) {
          this.processPage(res.data);
          this.processRowsView(res.data.data);

          this.fixedHeader.check = this.is.data; this.fixedHeader.getWidthForHeader();
        }
        this.is.loading = false;
      }
    )
  }
  error(res: ApiResponse<Pagging<RiskTaggingResponse[]>>) {
    var err = false;
    if (res.codeMessage.code) {
      this.alertService.showMessage("Error when get all risk tagging", res.codeMessage.message, MessageSeverity.error);
      err = true;
    }
    if (isNullOrUndefined(res.data.data)) {
      this.alertService.showMessage("Error when get all risk tagging", "Response data is null", MessageSeverity.error);
      err = true;
    }
    return err;
  }
  processPage(page: Pagging<any[]>) {
    this.is.data = page.data.length > 0;
    this.searchParams.page.index = page.currentPage;
    this.pagin.totalItems = page.totalItems;
    this.pagin.totalPages = page.totalPages;
  }
  processRowsView(data: RiskTaggingResponse[]) {
    this.rows = data;

    this.rowsView = [];
    for (var i = 0; i < data.length; i++)
      this.rowsView.push(new RiskTaggingView(data[i], this.positionTags, this.configService.timeZone));
  }

  ngOnDestroy() {
    this.fixedHeader.removeEventListener();
  }

  updateFieldList(isOpen: boolean) {
    if (isOpen)
      return;

    this.riskTaggingService.updateFieldList(RiskTaggingField.valueSendApi(this.fields))
      .subscribe(
        (res: ApiResponse<any>) => {
          if (res.codeMessage.code)
            return this.alertService.showMessage("Error when update field list", res.codeMessage.message, MessageSeverity.error);
        }
      )
  }

  search() {
    this.searchParams.page.index = 1;
    this.getRiskTag().subscribe();
  }

  pageChanged(event: { page: number, itemsPerPage: number }) {
    //pagechange 2 times cause by 2 element pagination
    if (event.page == this.searchParams.page.index) return;
    this.searchParams.page.index = event.page;
    this.getRiskTag().subscribe();
  }

  public progress: number;
  public message: string;
  @ViewChild('file') fileInputVariable: ElementRef;
  upload(files: any) {
    if (!files || files.length === 0)
      return;

    this.is.import = true;
    const formData = new FormData();
    formData.append(files.name, files);

    this.riskTaggingService.import(formData).subscribe(event => {
      console.log(event);

      if (event.type === HttpEventType.UploadProgress)
        this.progress = Math.round(100 * event.loaded / event.total);
      else if (event.type === HttpEventType.Response) {
        const res: ApiResponse<any> = event.body;

        if (res.codeMessage.code)
          return this.alertService.showMessage("Error when import file", res.codeMessage.message, MessageSeverity.error);
        this.message = res.codeMessage.message;
        timer(5000).subscribe(timer => {
          this.message = "";
        });
      }
    },
      (error) => this.message = JSON.stringify(error),
      () => {
        this.is.import = false;
        this.fileInputVariable.nativeElement.value = "";
        this.search();
      });
  }

  scrollToRight() {
    window.scrollTo({ left: window.outerWidth });
  }

  delete(index: number) {
    this.alertService.showDialog('Do you want to delete \"' + this.rows[index].fullName + '\"?', DialogType.confirm, () => {

      this.riskTaggingService.deleteRiskTagging(this.rows[index]).subscribe(
        (res: ApiResponse<null>) => {
          if (res.codeMessage.code)
            return this.alertService.showMessage("Error when delete risk tagging \"" + this.rows[index].fullName + "\"", res.codeMessage.message, MessageSeverity.error);
        }
        , null
        , () => this.getRiskTag()
      );

    });
  }

  editIndex: number = -2;
  editModel: RiskTaggingEdit;
  nowYear = new Date().getFullYear();
  minDate = new Date(this.nowYear - 100, 12, 31);
  edit(index: number) {
    this.editIndex = index;
    this.riskTaggingService.get(this.rows[index]).subscribe(
      (res: ApiResponse<RiskTaggingResponse>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage('Error when get Risk Tagging "' + this.rows[index].fullName + '"', res.codeMessage.message, MessageSeverity.error);

        this.rows[index] = res.data;
        this.rowsView[index] = new RiskTaggingView(res.data, this.positionTags, this.configService.timeZone);
        this.editModel = new RiskTaggingEdit(this.rows[index], this.configService.timeZone);
      }
      , null
      , () => this.openModal()
    )
  }
  cancelEdit() {
    this.editIndex = -2;
    this.is.saving = false;
    this.closeModal();
  }

  newRow() {
    this.editIndex = -1;
    this.editModel = new RiskTaggingEdit();
    this.openModal();
  }

  updateRow() {
    console.log("Update row");
    this.is.saving = true;
    this.newPT(
      () => this.updateRiskTagging()
    )
  }
  newPT(finish: () => void) {
    if (this.editModel.positionTagValue == -1)
      this.positionTagService.newObject(this.newPositionTag).subscribe(
        (res: ApiResponse<PositionTag>) => {
          if (res.codeMessage.code)
            return this.alertService.showMessage("Error when create new PT", res.codeMessage.message, MessageSeverity.error);

          this.positionTags.push(res.data);
          this.editModel.positionTagValue = this.newPositionTag.value;
          this.newPositionTag = new PositionTag(undefined, undefined);
        }
        , null
        , () => finish()
      );
    else
      finish();
  }
  updateRiskTagging() {
    if (this.editIndex > -1)
      this.riskTaggingService.editRiskTagging(this.editModel).subscribe(
        (res: ApiResponse<RiskTaggingResponse>) => {
          if (res.codeMessage.code)
            return this.alertService.showMessage('Error when save Risk Tagging "' + this.editModel.fullName + '"', res.codeMessage.message, MessageSeverity.error);

          this.rows[this.editIndex] = res.data;
          this.rowsView[this.editIndex] = new RiskTaggingView(res.data, this.positionTags, this.configService.timeZone);
        }
        , () => this.cancelEdit()
        , () => this.cancelEdit()
      )

    else if (this.editIndex == -1)
      this.riskTaggingService.newRiskTagging(this.editModel).subscribe(
        (res: ApiResponse<RiskTaggingResponse>) => {
          if (res.codeMessage.code)
            return this.alertService.showMessage("Error when create new risk tagging", res.codeMessage.message, MessageSeverity.error);
        }
        , () => this.cancelEdit()
        , () => {
          this.cancelEdit();
          this.getRiskTag();
        }
      )
  }

  @ViewChild(ModalDirective) editModal: ModalDirective;
  openModal() {
    this.editModal.show();
  }
  closeModal() {
    this.editModal.hide();
  }

  fileSaver: any = require('src/app/assets/scripts/FileSaver.min.js');
  exportAll() {
    this.is.exportAll = true;
    this.riskTaggingService.getExport(this.searchParams).subscribe(
      (res: Blob) => {
        const fileName = "RiskTagging_" + this.searchParams.bookie + "_" + Alg.nowString + ".xlsx";
        const file = new File([res], fileName);
        this.fileSaver.saveAs(file);
      }
      , () => this.is.exportAll = false
      , () => this.is.exportAll = false
    );

    this.search();
  }

  exportCur() {
    const data = RiskTaggingView.getExportData(this.rowsView);
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      //showLabels: true,
      //showTitle: true,
      //useBom: true,
      //noDownload: false,
      headers: RiskTaggingView.exportHeader
    };

    const fileName = "RiskTagging_" + this.searchParams.bookie + "_" + Alg.nowString + "_page" + this.searchParams.page.index + 'of' + this.pagin.totalPages;
    new Angular5Csv(data, fileName, options);
  }
}

