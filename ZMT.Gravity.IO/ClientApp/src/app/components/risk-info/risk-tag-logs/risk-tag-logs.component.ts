import { Component, OnInit, OnDestroy, AfterViewInit } from "@angular/core";
import { fadeInOut } from "src/app/services/animations";
import { RiskTagLogSearch } from "src/app/models/search.model";
import { RiskTagLogView, RiskTagLogResponse } from "src/app/models/risk-tag-log.model";
import { AlertService, MessageSeverity } from "src/app/services/alert.service";
import { RiskTagLogsService } from "src/app/services/risk-tag-logs.service";
import { ConfigurationService } from "src/app/services/configuration.service";
import { ApiResponse, Pagging } from "src/app/models/response.model";
import { UIState } from "src/app/models/ui-state.model";
import { PaginationView } from "src/app/models/pagination.model";
import { FixedHeader } from "src/app/models/fixed-header";
import { ActivatedRoute } from "@angular/router";
import { isNull, isNullOrUndefined } from "util";


@Component({
  selector: 'risk-tag-logs',
  templateUrl: './risk-tag-logs.component.html',
  styleUrls: ['./risk-tag-logs.component.css'],
  animations: [fadeInOut]
})

export class RiskTagLogsComponent implements OnInit, OnDestroy, AfterViewInit {

  rows: RiskTagLogResponse[] = [];
  rowsView: RiskTagLogView[] = [];
  searchParams: RiskTagLogSearch = new RiskTagLogSearch(this.configService.timeZone);
  is: UIState = { loading: true, data: false, exportAll: false, exportCur: false }
  pagin: PaginationView = new PaginationView();

  fixedHeader: FixedHeader = new FixedHeader('.rtl');

  constructor(
    private service: RiskTagLogsService
    , private configService: ConfigurationService
    , private alertService: AlertService
    , private route: ActivatedRoute) { }

  ngOnInit() {
    this.getSearchParamsFromUrl();
    this.getAll();
  }

  ngAfterViewInit() {
    this.fixedHeader.init();
    this.fixedHeader.addEventListener();
  }

  getSearchParamsFromUrl() {
    for (var key in this.searchParams) {
      const value = this.route.snapshot.queryParamMap.get(key);
      if (!isNull(value))
        this.searchParams[key] = value;
    }
  }

  getAll() {
    this.is.loading = true;
    this.is.data = false;

    this.service.getAll(this.searchParams).subscribe(
      (res: ApiResponse<Pagging<RiskTagLogResponse[]>>) => {
        if (!this.error(res)) {
          this.processPage(res.data);
          this.processRowsView(res.data.data);
        }

        this.fixedHeader.check = this.is.data; this.fixedHeader.getWidthForHeader();
        this.is.loading = false;
      }
    )
  }
  error(res: ApiResponse<Pagging<RiskTagLogResponse[]>>) {
    var err = false;
    if (res.codeMessage.code) {
      this.alertService.showMessage("Error when get all risk tag log", res.codeMessage.message, MessageSeverity.error);
      err = true;
    }
    if (isNullOrUndefined(res.data.data)) {
      this.alertService.showMessage("Error when get all risk tag log", "Response data is null", MessageSeverity.error);
      err = true;
    }
    return err;
  }
  processPage(page: Pagging<any[]>) {
    this.is.data = page.data.length > 0;
    this.searchParams.page.index = page.currentPage;
    this.pagin.totalItems = page.totalItems;
    this.pagin.totalPages = page.totalPages;
  }
  processRowsView(data: RiskTagLogResponse[]) {
    this.rows = data;

    this.rowsView = [];
    for (var i = 0; i < data.length; i++)
      this.rowsView.push(new RiskTagLogView(data[i], this.configService.timeZone));
  }

  ngOnDestroy() {
    this.fixedHeader.removeEventListener();
  }

  search() {
    this.searchParams.page.index = 1;
    this.getAll();
  }

  pageChanged(event: { page: number, itemsPerPage: number }) {
    //pagechange 2 times cause by 2 element pagination
    if (event.page == this.searchParams.page.index) return;
    this.searchParams.page.index = event.page;
    this.getAll();
  }
}
