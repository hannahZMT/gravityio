import { Component, OnInit, OnDestroy, AfterViewInit } from "@angular/core";
import { fadeInOut } from "src/app/services/animations";
import { BetResponse, BetView } from "src/app/models/bet.model";
import { BetsHistorySearch } from "src/app/models/search.model";
import { BetStatus } from "src/app/models/enums";
import { UIState } from "src/app/models/ui-state.model";
import { PaginationView } from "src/app/models/pagination.model";
import { FixedHeader } from "src/app/models/fixed-header";
import { BetsService } from "src/app/services/bets.service";
import { ConfigurationService } from "src/app/services/configuration.service";
import { AlertService, MessageSeverity } from "src/app/services/alert.service";
import { ActivatedRoute } from "@angular/router";
import { isNull, isNullOrUndefined } from "util";
import { ApiResponse, Pagging } from "src/app/models/response.model";
import { Angular5Csv } from "angular5-csv/Angular5-csv";
import { Alg } from 'src/app/models/alg';


@Component({
  selector: 'settled-bets',
  templateUrl: './settled-bets.component.html',
  styleUrls: ['./settled-bets.component.css'],
  animations: [fadeInOut]
})

export class SettledBetComponent implements OnInit, OnDestroy, AfterViewInit {

  rows: BetResponse[] = [];
  rowsView: BetView[] = [];
  searchParams: BetsHistorySearch = new BetsHistorySearch(this.configService.timeZone, BetStatus.SETTLED, true);
  is: UIState = { loading: true, data: false, exportAll: false, exportCur: false }
  pagin: PaginationView = new PaginationView();

  fixedHeader: FixedHeader = new FixedHeader('.settled-bets');
  isExportAll: boolean = false;
  isExportCur: boolean = false;

  constructor(
    private betsService: BetsService
    , private configService: ConfigurationService
    , private alertService: AlertService
    , private route: ActivatedRoute) { }

  ngOnInit() {
    this.getSearchParamsFromUrl();
    this.getAll();
  }

  ngAfterViewInit() {
    this.fixedHeader.init();
    this.fixedHeader.addEventListener();
  }

  getSearchParamsFromUrl() {
    for (var key in this.searchParams) {
      const value = this.route.snapshot.queryParamMap.get(key);
      if (!isNull(value))
        this.searchParams[key] = value;
    }
  }

  getAll() {
    this.is.loading = true;
    this.is.data = false;

    this.betsService.getAll(this.searchParams).subscribe(
      (res: ApiResponse<Pagging<BetResponse[]>>) => {
        if (!this.error(res)) {
          this.processPage(res.data);
          this.processRowsView(res.data.data);
        }

        this.fixedHeader.check = this.is.data; this.fixedHeader.getWidthForHeader();
        this.is.loading = false;
      }
    )
  }
  error(res: ApiResponse<Pagging<BetResponse[]>>) {
    var err = false;
    if (res.codeMessage.code) {
      this.alertService.showMessage("Error when get all settled bets", res.codeMessage.message, MessageSeverity.error);
      err = true;
    }
    if (isNullOrUndefined(res.data.data)) {
      this.alertService.showMessage("Error when get all settled bets", "Response data is null", MessageSeverity.error);
      err = true;
    }
    return err;
  }
  processPage(page: Pagging<any[]>) {
    this.is.data = page.data.length > 0;
    this.searchParams.page.index = page.currentPage;
    this.pagin.totalItems = page.totalItems;
    this.pagin.totalPages = page.totalPages;
  }
  processRowsView(data: BetResponse[]) {
    this.rows = data;

    this.rowsView = [];
    for (var i = 0; i < data.length; i++)
      this.rowsView.push(new BetView(data[i], this.configService.timeZone));
  }

  ngOnDestroy() {
    this.fixedHeader.removeEventListener();
  }

  search() {
    this.searchParams.page.index = 1;
    this.getAll();
  }

  pageChanged(event: { page: number, itemsPerPage: number }) {
    //pagechange 2 times cause by 2 element pagination
    if (event.page == this.searchParams.page.index) return;
    this.searchParams.page.index = event.page;
    this.getAll();
  }

  fileSaver: any = require('src/app/assets/scripts/FileSaver.min.js');
  exportAll() {
    this.is.exportAll = true;

    this.betsService.export(this.searchParams, true).subscribe(
      (res: Blob) => {
        const fileName = "SettledBets_" + this.searchParams.bookie + "_" + Alg.nowString + ".xlsx";
        const file = new File([res], fileName);
        this.fileSaver.saveAs(file);
      }
      , () => this.is.exportAll = false
      , () => this.is.exportAll = false
    );

    this.search();
  }

  exportCur() {
    const data = BetView.getExportData(BetStatus.SETTLED, this.rowsView, this.searchParams);
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      //showLabels: true,
      //showTitle: true,
      //useBom: true,
      //noDownload: false,
      headers: BetView.getExportHeader(BetStatus.SETTLED)
    };

    const fileName = "SettledBets_" + this.searchParams.bookie + "_" + Alg.nowString + "_page" + this.searchParams.page.index + 'of' + this.pagin.totalPages;
    new Angular5Csv(data, fileName, options);
  }
}