import { Component, Input, ViewChild, ElementRef } from "@angular/core";
import { fadeInOut } from "src/app/services/animations";
import { ViolationDetailReponse, ViolationDetailView, ViolationDetailEdit } from "src/app/models/violation-detail.model";
import { Bookie, EnumObj, EnumArray } from "src/app/models/enums";
import { PositionTag } from "src/app/models/position-tag.model";
import { AlertService, MessageSeverity } from "src/app/services/alert.service";
import { ViolationsService } from "src/app/services/violations.service";
import { ConfigurationService } from "src/app/services/configuration.service";
import { isNullOrUndefined } from "util";
import { ApiResponse } from "src/app/models/response.model";
import { MemberRiskTagResponse } from "src/app/models/member-risk-tag.model";
import { Angular5Csv } from "angular5-csv/Angular5-csv";
import { UIState } from "src/app/models/ui-state.model";
import { Alg } from 'src/app/models/alg';
import { PositionTagService } from "src/app/services/position-tag.service";


@Component({
  selector: 'violation-detail',
  templateUrl: './violation-detail.component.html',
  styleUrls: ['./violation-detail.component.css'],
  animations: [fadeInOut],
})

export class ViolationDetailComponent {
  @Input('violation') violation: { id: string, message: string }
  @Input('showButton') showButton: boolean = true;
  @Input('oneModal') oneModal: boolean = false;

  @ViewChild('butn')
  butn: ElementRef;

  dataEdit: ViolationDetailEdit = new ViolationDetailEdit(undefined);
  dataView: ViolationDetailView = { memberPT: undefined, violation: undefined }
  positionTags: EnumObj[];

  is: UIState = { loading: true, data: false, exportCur: false }

  constructor(
    private alertService: AlertService
    , private service: ViolationsService
    , private config: ConfigurationService
    , private positionTagService: PositionTagService
  ) { }

  getDetail() {
    this.is.loading = true;
    this.is.data = false;

    if (!isNullOrUndefined(this.violation.id))
      this.service.detail(this.violation.id).subscribe(
        (res: ApiResponse<ViolationDetailReponse>) => {
          this.dataEdit = new ViolationDetailEdit(res.data.memberPT);
          this.dataView = new ViolationDetailView(res.data.memberPT, res.data.violation, this.config.timeZone);

          this.is.data = res.data.violation.details.data.length > 0;

          if (!isNullOrUndefined(res.data.memberPT))
            this.getPositionTags();


        }
        , null
        , () => this.is.loading = false
      )
  }

  getPositionTags() {
    this.positionTags = [];
    this.positionTagService.getAll().subscribe(
      (res: ApiResponse<PositionTag[]>) => {
        if (res.codeMessage.code) {
          this.alertService.showMessage("Error when get all PT", res.codeMessage.message, MessageSeverity.error);
          return;
        }

        for (var i = 0; i < res.data.length; i++)
          this.positionTags.push(
            {
              value: res.data[i].value
              , name: res.data[i].name
              , css: "pt-text pt-text-" + res.data[i].name.replace(/\s+/g, '')
            });
      })
  }

  updatePT() {
    this.is.loading = true;

    this.dataEdit.memberPT.source = Bookie.PIN;
    this.dataEdit.memberPT.memberPt = this.getPositionTagName(this.dataEdit.memberPT.positionTagValue);
    this.service.updatePT(this.dataEdit.memberPT).subscribe(
      (res: ApiResponse<MemberRiskTagResponse>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage("Error when update PT of \"" + this.dataEdit.memberPT.memberFullName + "\"", res.codeMessage.message, MessageSeverity.error);
      }
      , null
      , () => this.is.loading = false
    )
  }

  getPositionTagName(value: number): string {
    for (var i = 0; i < this.positionTags.length; i++)
      if (this.positionTags[i].value == value) {
        return this.positionTags[i].name;
      }
  }

  toogleShow() {
    this.butn.nativeElement.click();
  }

  export() {
    this.is.exportCur = true;

    const data = ViolationDetailView.getExportData(this.dataView.violation.details.data);
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      headers: ViolationDetailView.exportHeader
    };

    const fileName = "ViolationDetail_" + Alg.nowString;
    new Angular5Csv(data, fileName, options);

    this.is.exportCur = false;
  }
}
