
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';

import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { ConfigurationService } from '../../services/configuration.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { BootstrapSelectDirective } from "../../directives/bootstrap-select.directive";
import { AccountService } from '../../services/account.service';
import { Utilities } from '../../services/utilities';
import { Permission } from '../../models/permission.model';
import { ConfigurationEndpoint } from 'src/app/services/configuration-endpoint.service';
import { UserSetting } from 'src/app/models/user-setting.model';


@Component({
  selector: 'user-preferences',
  templateUrl: './user-preferences.component.html',
  styleUrls: ['./user-preferences.component.css']
})
export class UserPreferencesComponent implements OnInit, OnDestroy {

  themeSelectorToggle = true;

  languageChangedSubscription: any;
  gmts: any;

  /* @ViewChild("languageSelector")
  languageSelector: BootstrapSelectDirective; */

  /* @ViewChild("homePageSelector")
  homePageSelector: BootstrapSelectDirective; */

  constructor(
    private alertService: AlertService
    , private translationService: AppTranslationService
    , private accountService: AccountService
    , private configEndpoint: ConfigurationEndpoint
    , public configurations: ConfigurationService) {
    const localTimeZone = -(new Date()).getTimezoneOffset() / 60;
    const sign = localTimeZone < 0 ? '' : '+';
    this.gmts = [
      { name: "Server Time (GMT)", gmt: 0 },
      { name: "Local Time (GMT " + sign + localTimeZone + ")", gmt: localTimeZone }
    ]
  }

  ngOnInit() {
    this.languageChangedSubscription = this.translationService.languageChanged$.subscribe(data => {
      this.themeSelectorToggle = false;

      setTimeout(() => {
        /* this.languageSelector.refresh();
        this.homePageSelector.refresh(); */
        this.themeSelectorToggle = true;
      });
    });
  }

  ngOnDestroy() {
    this.languageChangedSubscription.unsubscribe();
  }



  reloadFromServer() {
    this.alertService.startLoadingMessage();

    this.accountService.getUserPreferences()
      .subscribe(results => {
        this.alertService.stopLoadingMessage();

        this.configurations.import(results);

        this.alertService.showMessage("Defaults loaded!", "", MessageSeverity.info);

      },
        error => {
          this.alertService.stopLoadingMessage();
          this.alertService.showStickyMessage("Load Error", `Unable to retrieve user preferences from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
        });
  }

  setAsDefault() { //SET db API
    //console.log(this.configurations.time); gmt + ?
    this.alertService.showDialog("Are you sure you want to set the current configuration as your new defaults?", DialogType.confirm,
      () => this.setAsDefaultHelper(),
      () => this.alertService.showMessage("Operation Cancelled!", "", MessageSeverity.default));
  }

  setAsDefaultHelper() {
    this.alertService.startLoadingMessage("", "Saving new defaults");


    const data: UserSetting = { isServerTime: this.configurations.time == 0 ? true : false, theme: this.configurations.theme };
    this.configEndpoint.setConfig(JSON.stringify(data)).subscribe(
      (res) => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage("New Defaults", "Account defaults updated successfully", MessageSeverity.success)

      },
      error => {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", `An error occured whilst saving configuration defaults.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
          MessageSeverity.error, error);
      });

    /* this.accountService.updateUserPreferences(this.configurations.export())
      .subscribe(response => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage("New Defaults", "Account defaults updated successfully", MessageSeverity.success)
  
      },
        error => {
          this.alertService.stopLoadingMessage();
          this.alertService.showStickyMessage("Save Error", `An error occured whilst saving configuration defaults.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
        }); */
  }



  resetDefault() { //GET from API
    this.alertService.showDialog("Are you sure you want to reset your defaults?", DialogType.confirm,
      () => this.resetDefaultHelper(),
      () => this.alertService.showMessage("Operation Cancelled!", "", MessageSeverity.default));
  }

  resetDefaultHelper() {
    this.alertService.startLoadingMessage("", "Resetting defaults");

    this.accountService.updateUserPreferences(null)
      .subscribe(response => {
        this.alertService.stopLoadingMessage();
        this.configurations.import(null);
        this.alertService.showMessage("Defaults Reset", "Account defaults reset completed successfully", MessageSeverity.success)

      },
        error => {
          this.alertService.stopLoadingMessage();
          this.alertService.showStickyMessage("Save Error", `An error occured whilst resetting configuration defaults.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
        });
  }




  get canViewCustomers() {
    return this.accountService.userHasPermission(Permission.viewUsersPermission); //eg. viewCustomersPermission
  }

  get canViewProducts() {
    return this.accountService.userHasPermission(Permission.viewUsersPermission); //eg. viewProductsPermission
  }

  get canViewOrders() {
    return true; //eg. viewOrdersPermission
  }
}
