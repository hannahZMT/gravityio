import { Component, OnInit, OnDestroy, isDevMode, Input, AfterViewInit, ViewChild, ElementRef } from "@angular/core";

@Component({
  selector: "loading",
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css'],
  inputs: ['unit:unitname'],
})

export class LoadingComponent implements OnDestroy, AfterViewInit {
  loadingTime: number;
  unit: string = 'mins';
  audio = new Audio();
  isDevMode: boolean;

  @ViewChild("loadingScreen") loadingScreen: ElementRef<HTMLDivElement>;

  constructor() {
  }

  ngAfterViewInit() {
    this.isDevMode = isDevMode();
    if (this.isDevMode) {
      this.loadingTime = Date.now();
      this.audio.src = "https://freesound.org/data/previews/35/35643_14771-lq.mp3";
      this.audio.load();

      this.loadingScreen.nativeElement.style.marginTop = "50px";
    }
  }

  ngOnDestroy() {
    if (this.isDevMode) {
      this.audio.play();
      this.loadingTime = Date.now() - this.loadingTime;
      console.log("Loading time = " + this.loadingTime / 60000 + " " + this.unit);
    }
  }
}
