import { Component, OnInit, OnDestroy, ViewChild, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { fadeInOut } from '../../services/animations';
import { BetsHistoryService } from '../../services/bets-history.service';
import { Subscription, timer } from 'rxjs';
import { ToastyService, ToastyConfig, ToastOptions } from 'ng2-toasty';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { formatDate } from '@angular/common';
import * as $ from 'jquery';
import { ConfigurationService } from '../../services/configuration.service';
import { RealTimeBetView, RealTimeBetDb, RealTimeBetResponse } from 'src/app/models/realtimebet.model';
import { MemberRiskTagDb } from 'src/app/models/member-risk-tag.model';
import { Task } from 'src/app/models/task.model';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastaService } from 'ngx-toasta';
import { isNullOrUndefined } from 'util';
import { ViolationDetailComponent } from '../violation-detail/violation-detail.component';
import { ViolationDb } from 'src/app/models/violation.model';
import { ApiResponse } from 'src/app/models/response.model';
import { MessageSeverity, AlertService } from 'src/app/services/alert.service';
import { FixedHeader } from 'src/app/models/fixed-header';
import { UIState } from 'src/app/models/ui-state.model';
import { Alg } from 'src/app/models/alg';
import { Bookie } from 'src/app/models/enums';


@Component({
  selector: 'app-realtime-bets',
  templateUrl: './realtime-bets.component.html',
  styleUrls: ['./realtime-bets.component.css'],
  animations: [fadeInOut],
  encapsulation: ViewEncapsulation.None
})

export class RealTimeBetComponent implements OnInit, OnDestroy, AfterViewInit {
  fileSaver: any = require('../../assets/scripts/FileSaver.min.js');
  bookie = 'PIN';
  isExporting: boolean;
  columnIndex = { LastOdd: 5, D1: 6, D2: 6, D3: 6, MatchStatus: 8, }
  timeToGet = { LastOdd: 0, D1: 1, D2: 6, D3: 11 }
  showNotify: boolean = false;

  tbody: HTMLTableSectionElement;
  trs: HTMLCollection;

  socket: SocketIOClient.Socket;

  fixedHeader: FixedHeader = new FixedHeader(".realtime-bets");

  newestRowIndex: number = -1;

  _1min = 60 * 1000;
  _1hour = 60 * this._1min;
  reloadTime = 60 * 1000;         //s
  lostDataTime = 5 * 60 * 1000;   //5 mins
  is: UIState = { loading: true, init: true, data: false }
  timers: Subscription[] = [];

  //test
  divUnit = 1;           //5 mins ; set 1   -> prod
  logTimeFilter = 99999;     //mins   ; set max -> prod
  //test

  constructor(
    private BetsHistoryService: BetsHistoryService,
    private alertService: AlertService,
    private configService: ConfigurationService,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    private toastaService: ToastaService,
  ) {
    this.toastyConfig.position = "top-left";
    this.toastyConfig.showClose = false;
    this.toastyConfig.limit = 20;
  }

  ngOnInit() {
    this.tbody = document.getElementsByTagName("tbody")[0];

    this.getRealTimeBets(); //getAll -> ..listenChannel

    this.detail.violation = { id: undefined, message: undefined }
  }

  ngAfterViewInit() {
    this.fixedHeader.init();
    this.fixedHeader.addEventListener();
  }

  getRealTimeBets() {
    this.is.loading = true;
    this.is.data = false;

    this.BetsHistoryService.getRealTimeBets(this.bookie).subscribe(
      (res: ApiResponse<RealTimeBetResponse[]>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage("Error when get SmartRule for search", res.codeMessage.message, MessageSeverity.error);

        this.is.data = res.data.length > 0;

        for (var i = 0; i < res.data.length; i++) {
          var data = new RealTimeBetView(undefined, res.data[i], this.configService.timeZone);  //map funtion
          const tr = data.getRowElement;

          this.tbody.insertBefore(tr, undefined);
          this.updateRow(data);
        }

        if (!this.showNotify) this.toggle();
        this.is.loading = false;
      }
      , (error: HttpErrorResponse) => {
        this.is.loading = false;
        switch (error.status) {
          case 0:
            console.log("Lost connect API => Reload 10s");

            this.timers.push(timer(10000).subscribe(() => this.getRealTimeBets()));
            break;
          default:
            console.log("ERR => Reload 5s");
            this.timers.push(timer(5000).subscribe(() => this.getRealTimeBets()));
            break;

        }
      }
      , () => {
        this.fixedHeader.check = this.is.data; this.fixedHeader.getWidthForHeader();
      }
    )
  }

  setRoute(obj: RealTimeBetView, curTaskId: number
    , tasks: Task[]
  ): Subscription {
    if (curTaskId == tasks.length) return null;

    const waitTime = tasks[curTaskId].periodTime - tasks[curTaskId - 1].periodTime;
    console.log(obj.id + " "
      + (tasks[curTaskId].task).toString()
        .replace('function () { return _this.', '')
        .replace('(obj); }', '') + ": "
      + waitTime / this._1min + " mins");
    var nextTask: boolean;
    var t = timer(waitTime).subscribe(
      () => nextTask = tasks[curTaskId].task(obj)
      , null
      , nextTask == false ? null : () => this.setRoute(obj, curTaskId + 1, tasks));
    this.timers.push(t);
    return t;
  }

  updateRow(obj: RealTimeBetView) {
    const kickoffAt = (new Date(obj.eventDate)).valueOf();
    const betAt = (new Date(obj.betDate)).valueOf();
    const delta = kickoffAt - Date.now();

    var tasks: Task[] = [];

    tasks.push(new Task(null, Date.now()));
    if (obj.deltaOdds[0] == null)
      tasks.push(new Task(() => this.getD1(obj), betAt + this._1min));
    if (obj.deltaOdds[1] == null)
      tasks.push(new Task(() => this.getD2(obj), betAt + 6 * this._1min));
    if (obj.deltaOdds[2] == null)
      tasks.push(new Task(() => this.getD3(obj), betAt + 11 * this._1min));
    if (obj.lastOdds == null)
      tasks.push(new Task(() => this.getLastOdds(obj), kickoffAt));
    if (delta - this._1hour > 0)
      tasks.push(new Task(() => this.setStartingSoon(obj), kickoffAt - this._1hour));
    if (delta > 0)
      tasks.push(new Task(() => this.setInPlay(obj), kickoffAt));
    if (delta + 3 * this._1hour > 0)
      tasks.push(new Task(() => this.setUnknow(obj), kickoffAt + 3 * this._1hour));

    if (tasks.length == 1) return;

    for (var i = 0; i < tasks.length - 1; i++)
      for (var j = i + 1; j < tasks.length; j++)
        if (tasks[i].periodTime > tasks[j].periodTime) {
          const tmp = new Task(tasks[i].task, tasks[i].periodTime);
          tasks[i].task = tasks[j].task;
          tasks[i].periodTime = tasks[j].periodTime;

          tasks[j].task = tmp.task;
          tasks[j].periodTime = tmp.periodTime;
        }

    var route = obj.id + " : ";
    var beginTaskId = 1;
    for (var i = 0; i < tasks.length; i++)
      if (tasks[i].task == null) {
        beginTaskId = i + 1;
        break;
      } else
        tasks[i].task(obj); //lost data 

    for (var i = beginTaskId; i < tasks.length; i++)
      route += tasks[i].task.toString()
        .replace('function () { return _this.', '')
        .replace('(obj); }', '') + ":"
        + formatDate(new Date(tasks[i].periodTime), 'dd HH:mm:ss', 'en', this.configService.timeZone) + " -> ";

    console.log(route);
    this.timers.push(this.setRoute(obj, beginTaskId, tasks));
  }

  subNode() {
    this.connectNode('connect_error', environment.authenNode.authenChannel.realTimeBets);
    this.listenNewBet(environment.channels.realTimeBets.newBet);
    this.listenPT(environment.channels.realTimeBets.updatePT);
    this.listenViolation(environment.channels.realTimeBets.newViolation);
  }

  connectNode(err_channel: string, authen_channel: string) {
    this.socket = io.connect(environment.ip + ':' + environment.port); //secure -> config
    this.socket.on(err_channel, () => {
      console.log("node server lost connect, reload after " + this.reloadTime / this._1min + " mins");
      this.toggle();
      this.timers.push(timer(this.reloadTime).subscribe(() => {
        if (!this.showNotify)
          this.toggle();
      }));
    });
    this.socket.emit(authen_channel, environment.authenNode.inf);
  }

  listenNewBet(newBet_channel: string) {
    this.socket.on(newBet_channel,
      (res) => {
        const data: RealTimeBetDb = JSON.parse(res);
        this.addSuccessToasty("New Bet --> " + Bookie[data.Bookie] + "[" + data.UserNameCode + "/" + data.BookieRef + "] stake: " + data.Stake + " " + data.CurrencyCode);
        this.insertBetTimeDescending(data); // -> newestRowIndex
        const obj = new RealTimeBetView(data);
        this.updateRow(obj);
      });
  }

  listenPT(PT_channel: string) {
    this.socket.on(PT_channel,
      (res) => {
        var data: MemberRiskTagDb = JSON.parse(res);
        console.log("Member[" + data.UserName + "] SET PT = " + data.MemberPt);
        var trs = this.tbody.rows;
        for (var i = 0; i < 500; i++) {
          var td = trs[i].children[0];
          var div = td.children[3];
          if (div && td.innerHTML.indexOf(data.UserName) > 0) {
            div.innerHTML = data.MemberPt;
            this.trans(td, "#5cb85c", 256, 0);
          }
        }
      });
  }

  listenViolation(vio_channel: string) {
    this.setEventClick();

    this.socket.on(vio_channel,
      (res) => {
        var data: ViolationDb = JSON.parse(res);
        console.log(res);
        console.log(data.RuleId);

        //add toast>
        const toastOptions: ToastOptions = {
          title: data.RuleName,
          msg: "<button class=\"alert\" style=\"display:none\" r.Id=\"" + data.Id + "\">" + data.Message + "</button>" + data.Message,
          theme: 'bootstrap',
          timeout: 50000,
          showClose: true
        };
        switch (data.Severity) {
          case 1: this.toastaService.success(toastOptions);
            break;
          case 2: this.toastaService.warning(toastOptions);
            break;
          case 3: this.toastaService.error(toastOptions);
            break
        }
        //<add toast
      });
  }

  setEventClick() {
    const angularFun = this;
    $('.toasta-position-top-right').click(function (event) {
      var containtIdElement: Element;
      if (event.toElement.hasAttribute("r.Id"))
        containtIdElement = event.toElement                                          //click button
      else
        containtIdElement = event.toElement.getElementsByTagName("button")[0];       //click div

      if (!isNullOrUndefined(containtIdElement)) {
        const id = containtIdElement.getAttribute("r.Id");
        const message = containtIdElement.innerHTML;
        //show popup>
        angularFun.showDetail(id, message);
        //<show popup
      }
    })
  }

  insertBetTimeDescending(res: RealTimeBetDb) {
    this.trs = this.tbody.rows;
    var i: number;
    for (i = 0; i < this.trs.length; i++) {
      var betDate = new Date(res.BetDate);
      var rowDate = new Date(this.trs[i].getAttribute("r.betDate"));
      if (rowDate < betDate)
        break;
    }

    this.newestRowIndex = i;
    console.log("Add new : " + JSON.stringify(new RealTimeBetView(res)) + " at " + i);

    var row = (new RealTimeBetView(res)).getRowElement;
    this.tbody.insertBefore(
      row, this.trs[i])
      .setAttribute("style", "background-color: #ffe2ae");

    this.trans(row, "#ffe2ae", 255, 0);

    if (this.trs.length == 501)
      this.trs[500].remove();
  }

  trans(row: Element, baseColor: string, curColor: number, endColor: number) {
    if (curColor < endColor)
      timer(200).subscribe(() => {
        row.setAttribute("style", "background-color: " + baseColor + (endColor).toString(16));
      });
    else
      timer(200).subscribe(() => {
        row.setAttribute("style", "background-color: " + baseColor + (curColor).toString(16));
      }, null, () => this.trans(row, baseColor, curColor - 17, endColor));
  }

  ngOnDestroy(): void {
    if (this.socket) {
      this.showNotify = false;
      this.socket.disconnect();
    }
    for (var i = 0; i < this.timers.length; i++)
      if (this.timers[i] != null)
        this.timers[i].unsubscribe();

    this.toastyService.clearAll();
    this.fixedHeader.removeEventListener();
  }

  findRowPositionById(id: string): number {
    var pos: number = -1;
    this.trs = this.tbody.rows;
    for (var i = 0; i < 500; i++)
      if (this.trs[i].getAttribute("r.id") == id) {
        pos = i;
        break;
      }
    return pos;
  }

  getLastOdds(obj: RealTimeBetView): boolean {
    const kickoffAt = new Date(obj.eventDate);
    const delta = kickoffAt.valueOf() - Date.now().valueOf();
    if (delta + this.lostDataTime <= 0) {
      const pos = this.findRowPositionById(obj.id);
      if (pos == -1) {
        console.log(obj.id + ": removed!");
        return false;
      }
      console.log(obj.id + " Lost LastOdd!");
      this.trans(this.trs[pos].children[this.columnIndex.LastOdd], "#000000", 256, 128);
      return;
    }

    //getLastOdd
    const pos = this.findRowPositionById(obj.id);
    if (pos == -1) {
      console.log(obj.id + ": removed!");
      return false;
    }

    this.BetsHistoryService.getLastOdd(obj).subscribe(
      (res: ApiResponse<number>) => {
        var data: number = res.data;
        if (data == null) {
          console.log(obj.id + " GET LastOdd= null => getLastOdds: " + this.reloadTime / this._1min + " mins");
          return this.timers.push(timer(this.reloadTime).subscribe(() => this.getLastOdds(obj)));
        }

        console.log(obj.id + " GET LastOdd= " + this.trs[pos].children[this.columnIndex.LastOdd].children[0].innerHTML + " -> " + data);

        this.trs[pos].children[this.columnIndex.LastOdd].replaceChild(
          RealTimeBetView.lastOddsCellElement(data, obj.euroOdds, obj.market, obj.oddsType, obj.odds)
          , this.trs[pos].children[this.columnIndex.LastOdd].children[0]
        )


        this.trans(this.trs[pos].children[this.columnIndex.LastOdd], "#5cb85c", 256, 0);
      }
      , (error) => {
        console.log(obj.id + " GET LastOdd= " + error + " => getLastOdds: " + this.reloadTime / 2 / this._1min + " mins");
        if (error == "session expired (The specified refresh token is invalid.)") return this.ngOnDestroy();
        this.timers.push(timer(this.reloadTime / 2).subscribe(() => this.getLastOdds(obj)));
      }
    )
  }

  getD1(obj: RealTimeBetView): boolean {
    return this.getOdd(obj, 1);
  }

  getD2(obj: RealTimeBetView): boolean {
    return this.getOdd(obj, 2);
  }

  getD3(obj: RealTimeBetView): boolean {
    return this.getOdd(obj, 3);
  }

  getOdd(obj: RealTimeBetView, dIndex: 1 | 2 | 3): boolean {
    const betAt = new Date(obj.betDate);
    const delta = betAt.valueOf() - Date.now().valueOf() + this.timeToGet["D" + dIndex] * this._1min;
    if (delta + this.lostDataTime <= 0) {
      const pos = this.findRowPositionById(obj.id);
      if (pos == -1) {
        console.log(obj.id + ": removed!");
        return false;
      }

      console.log(obj.id + " Lost D" + dIndex + "!");
      this.trans(this.trs[pos].children[this.columnIndex["D" + dIndex]].children[dIndex - 1], "#000000", 256, 128);
      return;
    }

    const pos = this.findRowPositionById(obj.id);
    if (pos == -1) {
      console.log(obj.id + ": removed!");
      return false;
    }

    this.BetsHistoryService.getDeltaOdds(obj).subscribe(
      (res: ApiResponse<number[]>) => {
        const data: number = [res.data[0], res.data[1], res.data[2]][dIndex - 1];

        if (data == null) {
          console.log(obj.id + " GET D" + dIndex + "= null => getD" + dIndex + ": " + this.reloadTime / this._1min + " mins");
          return this.timers.push(timer(this.reloadTime).subscribe(() => this.getOdd(obj, dIndex)));
        }

        console.log(obj.id + " GET D" + dIndex + "= " + this.trs[pos].children[this.columnIndex["D" + dIndex]].children[dIndex - 1].innerHTML + " -> " + data);

        this.trs[pos].children[this.columnIndex["D" + dIndex]].replaceChild(
          RealTimeBetView.deltaOddDivElement(data, obj.euroOdds)
          , this.trs[pos].children[this.columnIndex["D" + dIndex]].children[dIndex - 1]
        )
        //this.trs[pos].children[this.columnIndex["D" + dIndex]] = obj.oddFieldElement(icon['d' + dIndex].icon.prefix, icon['d' + dIndex].icon.iconName, icon['d' + dIndex].style, data);
        this.trans(this.trs[pos].children[this.columnIndex["D" + dIndex]].children[dIndex - 1], "#5cb85c", 256, 0);
      }
      , (error) => {
        console.log(obj.id + " GET D" + dIndex + "= " + error + " => getD" + dIndex + ": " + this.reloadTime / 2 / this._1min + " mins");
        if (error == "session expired (The specified refresh token is invalid.)") return this.ngOnDestroy();
        this.timers.push(timer(this.reloadTime / 2).subscribe(() => this.getOdd(obj, dIndex)));
      }
    )
  }

  setStartingSoon(obj: RealTimeBetView): boolean {
    return this.setMatchStatusByObj(obj, "Starting soon");
  }

  setInPlay(obj: RealTimeBetView): boolean {
    return this.setMatchStatusByObj(obj, "In play");
  }

  setUnknow(obj: RealTimeBetView): boolean {
    return this.setMatchStatusByObj(obj, "");
  }

  setMatchStatusByObj(obj: RealTimeBetView, innerHTML: string): boolean {
    const pos = this.findRowPositionById(obj.id);
    if (pos == -1) {
      console.log(obj.id + ": removed!");
      return false;
    }
    const td = this.trs[pos].children[this.columnIndex.MatchStatus];
    td.innerHTML = innerHTML;
    this.trans(td, "#5cb85c", 256, 0);
    console.log(obj.id + " SET Match Status= " + td.innerHTML);
  }

  addSuccessToasty(mess: any) {
    var toastOptions: ToastOptions = {
      title: "",
      msg: mess,
      theme: 'bootstrap',
      timeout: 10000,
      showClose: true
    };
    this.toastyService.success(toastOptions);
  }

  toggle() {
    this.showNotify = !this.showNotify;
    if (this.showNotify) {
      console.log("Listening new bet..");
      this.subNode();
    } else {
      console.log("STOP listening new bet..");
      this.socket.disconnect();
    }
  }

  btnExport_Click() {
    this.isExporting = true;

    this.BetsHistoryService.getExport(this.bookie).subscribe(
      (res: Blob) => {
        const fileName = "RealTimeBets_" + this.bookie + "_" + Alg.nowString + ".xlsx";
        const file = new File([res], fileName);
        this.fileSaver.saveAs(file);
      }
      , () => this.isExporting = false
      , () => this.isExporting = false
    );
  }

  @ViewChild('detail')
  detail: ViolationDetailComponent;
  showDetail(id: string, message: string) {
    this.detail.violation.id = id;
    this.detail.violation.message = message;
    this.detail.toogleShow();
  }
}

//client have the same request -> cache to return calculated result

//null / lost data -> ERR
//is rem -> break route ; 
