import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from "@angular/core";
import { fadeInOut } from "src/app/services/animations";
import { ViolationResponse, ViolationView } from "src/app/models/violation.model";
import { ViolationSearch } from "src/app/models/search.model";
import { PaginationView } from "src/app/models/pagination.model";
import { FixedHeader } from "src/app/models/fixed-header";
import { SmartRuleResponse } from "src/app/models/smart-rule.model";
import { ViolationStatus, EnumArray } from "src/app/models/enums";
import { ViolationDetailComponent } from "../violation-detail/violation-detail.component";
import { ViolationsService } from "src/app/services/violations.service";
import { ConfigurationService } from "src/app/services/configuration.service";
import { AlertService, MessageSeverity } from "src/app/services/alert.service";
import { ActivatedRoute } from "@angular/router";
import { SmartRuleService } from "src/app/services/smart-rule.service";
import { isNull, isNullOrUndefined } from "util";
import { ApiResponse, Pagging } from "src/app/models/response.model";
import { Angular5Csv } from "angular5-csv/Angular5-csv";
import { UIState } from "src/app/models/ui-state.model";
import { Alg } from "src/app/models/alg";


@Component({
  selector: 'violations',
  templateUrl: './violations.component.html',
  styleUrls: ['./violations.component.css'],
  animations: [fadeInOut]
})

export class ViolationsComponent implements OnInit, OnDestroy, AfterViewInit {

  rows: ViolationResponse[] = [];
  rowsView: ViolationView[] = [];
  searchParams: ViolationSearch = new ViolationSearch(this.configService.timeZone);
  is: UIState = { loading: true, data: false, exportAll: false, exportCur: false, errors: undefined, saving: false }
  pagin: PaginationView = new PaginationView();

  fixedHeader: FixedHeader = new FixedHeader('.violations');

  ruleList: SmartRuleResponse[] = [];
  statuses = EnumArray.ViolationStatus;
  severities = EnumArray.SmartRuleSeverity;

  @ViewChild('detail')
  detail: ViolationDetailComponent;

  constructor(
    private violationsService: ViolationsService
    , private configService: ConfigurationService
    , private alertService: AlertService
    , private route: ActivatedRoute
    , private smartRuleService: SmartRuleService
  ) { }

  ngOnInit() {
    this.getSearchParamsFromUrl();
    this.getSmartRules();
    this.getAll();

    this.detail.violation = { id: undefined, message: undefined };
    this.detail.toogleShow();
  }

  ngAfterViewInit() {
    this.fixedHeader.init();
    this.fixedHeader.addEventListener();
  }

  getSearchParamsFromUrl() {
    console.log("get search");
    for (var key in this.searchParams) {
      const value = this.route.snapshot.queryParamMap.get(key);
      if (!isNull(value))
        this.searchParams[key] = value;
    }
  }

  getSmartRules() {
    this.smartRuleService.getAll().subscribe(
      (res: ApiResponse<SmartRuleResponse[]>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage("Error when get SmartRule for search", res.codeMessage.message, MessageSeverity.error);

        this.ruleList = res.data;
      }
    );
  }

  getAll() {
    this.is.loading = true;
    this.is.data = false;

    this.violationsService.getAll(this.searchParams).subscribe(
      (res: ApiResponse<Pagging<ViolationResponse[]>>) => {
        if (!this.error(res)) {
          this.processPage(res.data);
          this.processRowsView(res.data.data);
        }

        this.fixedHeader.check = this.is.data; this.fixedHeader.getWidthForHeader();
        this.is.loading = false;
      }
    )
  }
  error(res: ApiResponse<Pagging<ViolationResponse[]>>) {
    var err = false;
    if (res.codeMessage.code) {
      this.alertService.showMessage("Error when get all violations", res.codeMessage.message, MessageSeverity.error);
      err = true;
    }
    if (isNullOrUndefined(res.data.data)) {
      this.alertService.showMessage("Error when get all violations", "Response data is null", MessageSeverity.error);
      err = true;
    }
    return err;
  }
  processPage(page: Pagging<any[]>) {
    this.is.data = page.data.length > 0;
    this.searchParams.page.index = page.currentPage;
    this.pagin.totalItems = page.totalItems;
    this.pagin.totalPages = page.totalPages;
  }
  processRowsView(data: ViolationResponse[]) {
    this.rows = data;

    this.rowsView = [];
    for (var i = 0; i < data.length; i++)
      this.rowsView.push(new ViolationView(data[i], this.configService.timeZone));
  }

  ngOnDestroy() {
    this.fixedHeader.removeEventListener();
  }

  search() {
    this.searchParams.page.index = 1;
    this.getAll();
  }

  pageChanged(event: { page: number, itemsPerPage: number }) {
    //pagechange 2 times cause by 2 element pagination
    if (event.page == this.searchParams.page.index) return;
    this.searchParams.page.index = event.page;
    this.getAll();
  }

  showDetail(index: number) {
    this.detail.violation.id = this.rows[index].id;
    this.detail.violation.message = this.rows[index].message;
    this.detail.toogleShow();
  }
  editRowId: number = -1;
  setEditRow(index: number) {

    this.rows[index].status = ViolationStatus[this.rowsView[index].status];
    this.rows[index].notes = this.rowsView[index].notes;
    this.editRowId = index;

    this.fixedHeader.check = this.is.data; this.fixedHeader.getWidthForHeader(true);
  }
  cancelEdit() {
    this.editRowId = -1;

    this.fixedHeader.check = this.is.data; this.fixedHeader.getWidthForHeader(true);
  }
  updateRow(index: number) {
    this.is.saving = true;
    this.alertService.startLoadingMessage("Saving...", this.rowsView[index].ruleName);

    this.violationsService.update(this.rows[index]).subscribe(
      (res: ApiResponse<ViolationResponse[]>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage("Error when saving violation \"" + this.rows[index].message, res.codeMessage.message, MessageSeverity.error);

        this.updateAll(res.data);
        this.cancelEdit();

        this.alertService.stopLoadingMessage();
        this.alertService.showMessage("Saved successfully", this.rowsView[index].ruleName, MessageSeverity.success)

        this.is.saving = false;
      }
      //, () => this.editRowId = -1
    );
  }
  updateAll(res: ViolationResponse[]) {
    for (var i = 0; i < this.rows.length; i++)
      for (var j = 0; j < res.length; j++)
        if (this.rows[i].id == res[j].id) {
          this.rows[i] = res[j];
          this.rowsView[i] = new ViolationView(res[j], this.configService.timeZone);
        }
  }

  exportCur() {
    this.is.exportCur = true;

    const data = ViolationView.getExportData(this.rowsView);
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      headers: ViolationView.exportHeader
    };

    const fileName = "Violations_" + Alg.nowString + "_page" + this.searchParams.page.index + 'of' + this.pagin.totalPages;
    new Angular5Csv(data, fileName, options);

    this.is.exportCur = false;
  }
}
