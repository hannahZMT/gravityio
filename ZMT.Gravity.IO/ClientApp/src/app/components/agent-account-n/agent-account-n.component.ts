import { Component, ViewChild, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { fadeInOut } from '../../services/animations';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { timer, Subscription } from 'rxjs';

import { AgentAccountResponse, AgentAccountView, AgentAccountEdit, Captcha } from '../../models/agent-account.model';
import * as io from 'socket.io-client';
import { EnumArray, AgentAccountStatus } from "../../models/enums";
import { AgentAccountNService } from 'src/app/services/agent-account-n.service';
import { AgentAccountSearch } from 'src/app/models/search.model';
import { UIState } from 'src/app/models/ui-state.model';
import { PaginationView } from 'src/app/models/pagination.model';
import { Pagging, ApiResponse } from 'src/app/models/response.model';
import { isNullOrUndefined, isUndefined } from 'util';
import { NgForm } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { FixedHeader } from 'src/app/models/fixed-header';
import { Alg } from 'src/app/models/alg';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';

@Component({
  selector: 'agent-account-n',
  templateUrl: './agent-account-n.component.html',
  styleUrls: ['./agent-account-n.component.css'],
  animations: [fadeInOut]
})
export class AgentAccountNComponent implements OnInit, OnDestroy, AfterViewInit {

  rows: AgentAccountResponse[];
  rowsView: AgentAccountView[];
  searchParams: AgentAccountSearch = new AgentAccountSearch();
  is: UIState = { loading: true, data: false, exportCur: false }
  pagin: PaginationView = new PaginationView();

  fixedHeader: FixedHeader = new FixedHeader('.agent-account-n');

  bookies = EnumArray.Bookie;
  socket: SocketIOClient.Socket;
  _1min = 60 * 1000;
  reloadTime = 60 * 1000;

  constructor(
    private service: AgentAccountNService
    , private alertService: AlertService) {
  }

  ngOnInit() {
    this.loadData(true);
  }

  ngAfterViewInit() {
    this.fixedHeader.init();
    this.fixedHeader.addEventListener();
  }

  search() {
    this.searchParams.page.index = 1;
    this.loadData();
  }

  getAll() {
    this.is.loading = true;

    return this.service.getAll(this.searchParams).map(
      (res: ApiResponse<Pagging<AgentAccountResponse[]>>) => {
        if (this.error(res)) return;

        this.rows = res.data.data;
        this.processPage(res.data);
        this.processRowsView(res.data.data);

        this.fixedHeader.check = this.is.data;
        this.is.loading = false;
      })
  }
  error(res: ApiResponse<Pagging<AgentAccountResponse[]>>) {
    var err = false;
    if (res.codeMessage.code) {
      this.alertService.showMessage("Error when get all Agent Account", res.codeMessage.message, MessageSeverity.error);
      err = true;
    }
    if (isNullOrUndefined(res.data.data)) {
      this.alertService.showMessage("Error when get all Agent Account", "Response data is null", MessageSeverity.error);
      err = true;
    }
    if (res.data.currentPage > res.data.totalPages && res.data.totalPages > 0) {
      this.alertService.showMessage("Page " + res.data.currentPage + " does not exist", "Loading page " + res.data.totalPages, MessageSeverity.error);

      this.searchParams.page.index = res.data.totalPages;
      this.search();
      err = true;
    }
    return err;
  }
  processPage(page: Pagging<any[]>) {
    this.is.data = page.data.length > 0;
    this.searchParams.page.index = page.currentPage;
    this.pagin.totalItems = page.totalItems;
    this.pagin.totalPages = page.totalPages;
  }
  processRowsView(res: AgentAccountResponse[]) {
    this.is.data = res.length > 0;
    this.rowsView = [];
    for (var i = 0; i < res.length; i++)
      this.rowsView.push(new AgentAccountView(res[i]));
  }

  getFirstInputElement(): HTMLInputElement {
    var trs = $(".agent-account-n tbody tr");

    var res: HTMLInputElement = undefined;
    for (var i = 0; i < trs.length; i++) {
      res = trs[i].getElementsByTagName("input")[0];
      if (!isUndefined(res)) return res;
    }

    return res;
  }

  focusFirstInput() {
    this.timers.push(timer(10).subscribe(() => {
      const first = this.getFirstInputElement();
      if (!isUndefined(first)) first.focus();
      console.log(first);
    }))
  }

  timers: Subscription[] = [];
  subNode() {
    this.connectNode('connect_error', environment.authenNode.authenChannel.agentAccounts);
    this.listenAccountCaptcha(environment.channels.agentAccounts);
  }
  connectNode(err_channel: string, authen_channel: string) {
    this.socket = io.connect(environment.ip + ':' + environment.port); //secure -> config
    this.socket.on(err_channel, () => {
      console.log("node server lost connect, reload after " + this.reloadTime / this._1min + " mins");
      this.socket.disconnect();
      this.timers.push(
        timer(this.reloadTime).subscribe(() => {
          this.subNode();
        }));
    });
    this.socket.emit(authen_channel, environment.authenNode.inf);
  }
  listenAccountCaptcha(captcha_channel: string) {
    this.socket.on(captcha_channel,
      (res) => {
        console.log(res);
        if (res == "null") return;
        const data: Captcha = JSON.parse(res);
        var captchaInputLength = 0;

        for (var r of this.rowsView) {
          if (r.userName === data.UserName && r.source === data.Source)
            r.updateSttFromNode(data);
          if (r.agentStatus == AgentAccountStatus.Pause)
            captchaInputLength++;
        }

        if (captchaInputLength == 1)
          this.focusFirstInput();

        this.fixedHeader.getWidthForHeader();
      });
  }

  cancelEdit() {
    this.editIndex = -2;
    this.is.saving = false;
    this.closeModal();

  }

  editIndex = -2;
  editModel: AgentAccountEdit;
  edit(index: number) {
    this.editIndex = index;
    this.service.getObj(this.rows[index].key).subscribe(
      (res: ApiResponse<AgentAccountResponse>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage('Error when get Agent Account "' + this.rows[index].key + '"', res.codeMessage.message, MessageSeverity.error);

        this.rows[index] = res.data;
        this.rowsView[index] = new AgentAccountView(res.data);
        this.rowsView[index].updateStt(res.data);
        this.editModel = new AgentAccountEdit(res.data);
      }
      , null
      , () => this.openModal()
    )
  }
  @ViewChild("f") private form: NgForm;
  newRow() {
    this.form.form.markAsPristine();
    this.form.form.markAsUntouched();
    this.form.form.updateValueAndValidity();

    this.editIndex = -1;
    this.editModel = new AgentAccountEdit();
    this.openModal();
  }

  save() {
    if (this.editIndex > -1)
      this.updObj().subscribe(() => this.cancelEdit());

    else if (this.editIndex == -1)
      this.addObj().subscribe(() => { this.cancelEdit(); this.ngOnInit() });
  }

  updObj() {
    return this.service.updObj(this.editModel).map(
      (res: ApiResponse<AgentAccountResponse>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage('Error when save Agent Account "' + this.editModel.key + '"', res.codeMessage.message, MessageSeverity.error);

        this.rows[this.editIndex] = res.data;
        this.rowsView[this.editIndex] = new AgentAccountView(res.data);
      }
    )
  }

  addObj() {
    return this.service.addObj(this.editModel).map(
      (res: ApiResponse<AgentAccountResponse>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage("Error when create new Agent Account", res.codeMessage.message, MessageSeverity.error);
      }
    )
  }

  delete(index: number) {
    const obj = this.rows[index];
    this.alertService.showDialog('Are you sure you want to delete \"' + obj.key + '\"?', DialogType.confirm,
      () => this.delObj(obj).subscribe());
  }

  delObj(obj: AgentAccountResponse) {
    return this.service.delObj(obj.key).map(
      (res: ApiResponse<AgentAccountResponse>) => {

        if (res.codeMessage.code)
          return this.alertService.showMessage("Error when delete Agent Account \"" + res.data.key + "\"", res.codeMessage.message, MessageSeverity.error);

        this.ngOnInit();
      }
    )
  }

  @ViewChild("editModal") editModal: ModalDirective;
  openModal() {
    this.editModal.show();
  }
  closeModal() {
    this.editModal.hide();
  }

  autocompleteDomain() {
    this.editModel.domain = environment.bookieUrls[this.editModel.source];
  }

  exportCur() { //remove password
    const data = AgentAccountView.getExportData(this.rowsView);
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      headers: AgentAccountView.getExportHeader()
    };

    const fileName = "AgentAccounts_" + this.searchParams.model.source + "_" + Alg.nowString + "_page" + this.searchParams.page.index + 'of' + this.pagin.totalPages;
    new Angular5Csv(data, fileName, options);
  }

  ngOnDestroy() {
    if (this.socket)
      this.socket.disconnect();
    for (var i = 0; i < this.timers.length; i++)
      if (!this.timers[i].closed)
        this.timers[i].unsubscribe();

    this.fixedHeader.removeEventListener();
  }

  changeStt(index: number) {//lnkChangeStatusClick
    var acc = this.rowsView[index];

    let requestStatus = -1;

    if (acc.agentStatus === AgentAccountStatus.Running) {
      requestStatus = AgentAccountStatus.WaitForStop;
    } else if (acc.agentStatus === AgentAccountStatus.Stop) {
      requestStatus = AgentAccountStatus.WaitForRun;
    }
    acc.agentStatus == requestStatus;
    acc.agentStatusView = AgentAccountStatus[acc.agentStatus];
    acc.css_agentStatus = "agent-status-" + acc.agentStatusView;

    if (requestStatus > -1)
      this.service.changeAgentStatus(acc.source, acc.userName, requestStatus).subscribe(
        (res: any) => {//handle
          console.log(res);
          acc.agentStatus = res.agentStatus;
          acc.agentStatusView = AgentAccountStatus[acc.agentStatus];
          acc.css_agentStatus = "agent-status-" + acc.agentStatusView;
        }
        , () => {
          acc.agentStatus = AgentAccountStatus.Pause;
          acc.agentStatusView = "Error";
          acc.css_agentStatus = "agent-status-" + acc.agentStatusView;
        }
      );
  }

  resolve(index: number, focusNext?: boolean) {
    var acc = this.rowsView[index];
    this.service.setCaptchaByUserAccount(acc.source, acc.userName, acc.captchaText)
      .subscribe((res: any) => {//handle
        console.log(res);
      });


    acc.captchaText = '';
    acc.agentStatus = AgentAccountStatus.Running;
    acc.agentStatusView = AgentAccountStatus[acc.agentStatus];
    acc.css_agentStatus = "agent-status-" + acc.agentStatusView;

    if (focusNext) this.focusNextInput(index);
  }

  getNextAccInputElement(index: number): HTMLInputElement {
    var trs = $(".agent-account-n tbody tr");

    var res: HTMLInputElement = undefined;
    for (var i = index + 1; i < trs.length; i++) {
      res = trs[i].getElementsByTagName("input")[0];
      if (!isUndefined(res)) return res;
    }

    for (var i = 0; i < trs.length; i++) {
      res = trs[i].getElementsByTagName("input")[0];
      if (!isUndefined(res)) return res;
    }

    return res;
  }

  focusNextInput(index: number) {
    const next = this.getNextAccInputElement(index);
    if (!isUndefined(next)) next.focus();
  }


  isStartAll: boolean = false;
  startAll() {
    this.isStartAll = true;
    this.runAgentAccount(0, this.searchParams.model.source);
  }
  delayTimeToRunAcc = 2500;
  runAgentAccount(curId: number, bookie: string) {
    if (curId == this.rowsView.length) {
      this.isStartAll = false;
      return null;
    }

    if (this.rowsView[curId].agentStatus == AgentAccountStatus.Stop
      && (this.rowsView[curId].source == bookie || bookie == "All")) {
      var t = timer(this.delayTimeToRunAcc).subscribe(() => {
        console.log(this.rowsView[curId].userName + " : Stop -> Running");
        this.changeStt(curId);
      }
        , null
        , () => this.runAgentAccount(curId + 1, bookie));
      this.timers.push(t);
      return t;
    }
    return this.runAgentAccount(curId + 1, bookie);
  }

  isStopAll: boolean = false;
  stopAll() {
    this.isStopAll = true;
    for (var i = 0; i < this.rowsView.length; i++)
      if (this.rowsView[i].agentStatus == AgentAccountStatus.Running
        && (this.rowsView[i].source == this.searchParams.model.source || this.searchParams.model.source == "All")) {
        console.log(this.rowsView[i].userName + " : Running -> Stop");
        this.changeStt(i);
      }
    this.isStopAll = false;
  }

  pageChanged(event: { page: number, itemsPerPage: number }) {
    //pagechange 2 times cause by 2 element pagination
    if (event.page == this.searchParams.page.index) return;

    this.searchParams.page.index = event.page;
    this.loadData();
  }

  loadData(subNode?: boolean) {
    this.is.loading = true;

    this.getAll().subscribe(() => {
      if (subNode) this.subNode();
      this.fixedHeader.getWidthForHeader();
      this.focusFirstInput();
      this.is.loading = false;
    });
  }
}
