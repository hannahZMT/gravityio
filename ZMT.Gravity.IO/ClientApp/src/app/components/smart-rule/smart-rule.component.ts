/*
npm install install bootstrap-toggle
http://www.bootstraptoggle.com/
+ BootstrapToggleDirective

https://www.npmjs.com/package/ngx-chips
*/

import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { fadeInOut } from '../../services/animations';
import { SmartRuleService } from 'src/app/services/smart-rule.service';
import { SmartRuleResponse, SmartRuleView, SmartRuleEdit } from 'src/app/models/smart-rule.model';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { SmartRuleSeverity, EnumArray } from 'src/app/models/enums';
import { AlertService, MessageSeverity } from 'src/app/services/alert.service';
import { ApiResponse } from 'src/app/models/response.model';
import { FormControl } from '@angular/forms';
import { Alg } from 'src/app/models/alg';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UIState } from 'src/app/models/ui-state.model';
import { isNullOrUndefined } from 'util';
import { FixedHeader } from 'src/app/models/fixed-header';

@Component({
  selector: 'app-smart-rule',
  templateUrl: './smart-rule.component.html',
  styleUrls: ['./smart-rule.component.css'],
  animations: [fadeInOut]
})

export class SmartRuleComponent implements OnInit, OnDestroy, AfterViewInit {

  rows: SmartRuleResponse[] = [];
  rowsView: SmartRuleView[] = [];
  fixedHeader: FixedHeader = new FixedHeader('.sr-list');

  statuses: string[] = [];
  severities = EnumArray.SmartRuleSeverity;

  is: UIState = { loading: true, saving: false }

  constructor(
    private smartRuleService: SmartRuleService
    , private configService: ConfigurationService
    , private alertService: AlertService) { }

  ngOnInit() {
    this.getSmartRule();
  }

  ngOnDestroy() {
    this.fixedHeader.removeEventListener();
  }

  ngAfterViewInit() {
    this.fixedHeader.init();
    this.fixedHeader.addEventListener();
  }

  getSmartRule() {
    this.is.loading = true;

    this.smartRuleService.getAll().subscribe(
      (res: ApiResponse<SmartRuleResponse[]>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage("Error when get all Smart Rule", res.codeMessage.message, MessageSeverity.error);

        this.rows = res.data;
        for (var i = 0; i < res.data.length; i++) {
          var row = new SmartRuleView(res.data[i], this.configService.timeZone);
          this.rowsView.push(row);
        }

        this.is.data = res.data.length > 0;
        this.fixedHeader.check = this.is.data; this.fixedHeader.getWidthForHeader(true);
      }
      , () => this.is.loading = false
      , () => this.is.loading = false
    );
  }

  getSeverityName(id: SmartRuleSeverity): string {
    return SmartRuleSeverity[id];
  }

  editIndex: number = -2;
  editModel: SmartRuleEdit;
  editIPsModel: string[][];
  @ViewChild("editModal") editModal: ModalDirective;
  @ViewChild("editIPsModal") editIPsModal: ModalDirective;
  edit(index: number) {
    this._filter = '';
    this.editIndex = index;
    this.smartRuleService.get(this.rows[index]).subscribe(
      (res: ApiResponse<SmartRuleResponse>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage('Error when get Smart Rule "' + this.rows[index].ruleName + '"', res.codeMessage.message, MessageSeverity.error);

        this.rows[index] = res.data;
        this.rowsView[index] = new SmartRuleView(res.data, this.configService.timeZone);
        this.editModel = new SmartRuleEdit(res.data);
      }
      , null
      , () => this.editModal.show()
    )
  }
  col: number;
  ipRows: string[];
  ipRowsCache: string[];
  editIPs(ips: string[]) {
    this.editModal.hide();

    this.col = Math.floor(window.innerWidth * 0.8 / 200);
    this._filter = "";
    this.ipErrors = { 'required': true }

    this.ipRowsCache = [];
    for (var i = 0; i < ips.length; i++)
      this.ipRowsCache.push(ips[i]);
    this.updateIPList();

    this.editIPsModal.show()
  }
  to2Arr(ips: string[]) {
    this.editIPsModel = [];
    for (var i = 0; i < ips.length; i++) {
      var rowIndex = Math.floor(i / this.col);
      if (i % this.col == 0) {
        this.editIPsModel[rowIndex] = [];
        this.editIPsModel[rowIndex].push(ips[i]);
      }
      else {
        this.editIPsModel[rowIndex].push(ips[i]);
      }
    }
  }
  cancelEdit() {
    this.editIndex = -2;
    this.is.saving = false;
    this.editModal.hide();
  }

  addIP(ip: string) {
    if (this.checkIPexist(ip, this.ipRowsCache) || (ip == "") || this.checkIPValid(ip)) return;
    this.ipRowsCache.push(ip);
    this.updateIPList();
  }
  deleteIPs(ip: string) {
    var index = -1;
    for (var i = 0; i < this.ipRowsCache.length; i++)
      if (ip == this.ipRowsCache[i]) {
        index = i;
        break;
      }
    if (index == -1) return;

    for (var i = index; i < this.ipRowsCache.length - 1; i++)
      this.ipRowsCache[i] = this.ipRowsCache[i + 1];
    this.ipRowsCache.pop();
    this.updateIPList();
  }
  deleteAllIPs() {
    console.log("delele all");
    this.ipRowsCache = [];
    this.ipRows = [];
    this.editIPsModel = [];
  }
  confirmIPs() {
    for (var i = 0; i < this.editModel.settings.length; i++)
      if (this.editModel.settings[i].type == 'ips')
        this.editModel.settings[i].value = this.ipRowsCache;
    this.back();
    return;
  }
  back() {
    this.editIPsModal.hide();
    this.editModal.show();
  }
  updateIPList() {
    this.ipRows = this.ipRowsCache.filter(i => i.indexOf(this._filter) !== -1);
    this.to2Arr(this.ipRows);
  }

  updateRow() {
    if (this.editIndex <= -1) return;
    this.is.saving = true;

    this.smartRuleService.update(this.editModel).subscribe(
      (res: ApiResponse<SmartRuleResponse>) => {
        if (res.codeMessage.code)
          return this.alertService.showMessage('Error when save Smart Rule "' + this.editModel.ruleName + '"', res.codeMessage.message, MessageSeverity.error);

        this.rows[this.editIndex] = res.data;
        this.rowsView[this.editIndex] = new SmartRuleView(res.data, this.configService.timeZone);
      }
      , () => this.cancelEdit()
      , () => this.cancelEdit()
    );
  }

  fixStruct(e: { display: string, value: string }, values: any[]) {
    //check exist
    if (this.checkIPexist(e.value, values)) return;

    values.push(e.value);
  }

  _filter: string;
  checkIPexist(ip: string, ips: string[]) {
    for (var i = 0; i < ips.length; i++)
      if (ip == ips[i]) {
        this.ipErrors = { 'exist': true }
        return true;
      }

    return false;
  }
  checkAndUpdateIP(keyEvent: KeyboardEvent) {
    if (keyEvent.key == 'Enter') return;
    this.ipErrors = this.checkIPValid(this._filter); this.updateIPList()
  }

  ipErrors;
  checkIPValid(ip: string) {
    if (isNullOrUndefined(ip)) return;
    if (ip == "") return {
      'required': true
    }

    const ipPartString: string[] = ip.split('.', 5);
    const endFor = ipPartString.length > 4 ? 4 : ipPartString.length;

    for (var i = 0; i < endFor; i++) {
      if (!Alg.isNumber(ipPartString[i]))
        return {
          'num': true
        }

      const ipPart = Number.parseInt(ipPartString[i]);
      if (ipPart < 0 || ipPart > 255)
        return {
          'ipRange': true
        }
    }

    if (ipPartString.length != 4)
      return {
        'missingPoint': true
      }

    return null;
  }

  isIPv4(control: FormControl) {
    //cannot aplly this.checkIPValid because missing ref
    const ip: string = control.value;
    const ipPartString: string[] = ip.split('.', 5);
    const endFor = ipPartString.length > 4 ? 4 : ipPartString.length;

    for (var i = 0; i < endFor; i++) {
      if (!Alg.isNumber(ipPartString[i]))
        return {
          'num': true
        }

      const ipPart = Number.parseInt(ipPartString[i]);
      if (ipPart < 0 || ipPart > 255)
        return {
          'ipRange': true
        }
    }

    if (ipPartString.length != 4)
      return {
        'missingPoint': true
      }

    return null;
  }

  errs = {
    'missingPoint': 'Not IPv4 struct',
    'num': 'IP is number',
    'ipRange': 'IP ∈ [0,255]'
  }
}
