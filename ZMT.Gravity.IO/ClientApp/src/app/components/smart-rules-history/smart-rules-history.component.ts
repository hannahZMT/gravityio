import { Component, Input, ViewChild, OnChanges, ElementRef } from '@angular/core';
import { fadeInOut } from '../../services/animations';
import { SmartRuleService } from 'src/app/services/smart-rule.service';
import { SmartRuleResponse } from 'src/app/models/smart-rule.model';
import { SmartRulesHistoryResponse, SmartRulesHistoryView } from 'src/app/models/smart-rules-history.model';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { isNullOrUndefined } from 'util';
import { ApiResponse } from 'src/app/models/response.model';
import { AlertService, MessageSeverity } from 'src/app/services/alert.service';

@Component({
  selector: 'smart-rules-history',
  templateUrl: './smart-rules-history.component.html',
  styleUrls: ['./smart-rules-history.component.css'],
  animations: [fadeInOut],
})

export class SmartRulesHistoryComponent implements OnChanges {
  @Input('smartRule') smartRule: SmartRuleResponse;
  @Input('showButton') showButton: boolean = true;
  @Input('oneModal') oneModal: boolean = false;

  @ViewChild('butn')
  butn: ElementRef;

  data: SmartRulesHistoryView[];

  constructor(
    private alertService: AlertService
    , private smartRuleService: SmartRuleService
    , private configService: ConfigurationService
  ) { }

  getLogs() {
    console.log(this.smartRule);
    if (!isNullOrUndefined(this.smartRule.ruleId))
      this.smartRuleService.logs(this.smartRule.ruleId).subscribe(
        (res: ApiResponse<SmartRulesHistoryResponse[]>) => {
          if (res.codeMessage.code)
            return this.alertService.showMessage("Error when get all smartrule", res.codeMessage.message, MessageSeverity.error);

          this.data = [];
          for (var i = 0; i < res.data.length; i++)
            if (res.data[i].auditValues[0].columnName)
              this.data.push(new SmartRulesHistoryView(res.data[i], this.configService.timeZone))

        }
        , () => console.log("ERR")
        //, () => { if (this.oneModal) this.butn.nativeElement.click() }
      )
  }

  ngOnChanges() {
    /* if (this.butn)
      this.butn.nativeElement.click(); */
  }

  toogleShow() {
    this.butn.nativeElement.click();
  }
}
