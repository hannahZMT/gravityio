import { Injectable } from '@angular/core';
import { AgentAccountNEndpoint } from './agent-account-n-endpoint.service';
import { AgentAccountSearch } from '../models/search.model';
import { AgentAccountEdit } from '../models/agent-account.model';
import { AgentAccountStatus } from '../models/enums';
import { Observable } from 'rxjs';

@Injectable()
export class AgentAccountNService {

  constructor(private endpoint: AgentAccountNEndpoint) { }

  getAll(searchParams: AgentAccountSearch) {
    return this.endpoint.getAll(searchParams);
  }

  getObj(key: string) {
    return this.endpoint.getObj(key);
  }

  addObj(agentAccount: AgentAccountEdit) {
    return this.endpoint.addObj(agentAccount);
  }

  delObj(key: string) {
    return this.endpoint.delObj(key);
  }

  updObj(agentAccount: AgentAccountEdit) {
    return this.endpoint.updObj(agentAccount);
  }


  changeAgentStatus(bookie: string, username: string, newStatus: AgentAccountStatus): Observable<any> {
    return this.endpoint.changeAgentStatus(bookie, username, newStatus);
  }

  setCaptchaByUserAccount(bookie: string, userAccount: string, captchaText: string) {
    return this.endpoint.setCaptchaByUserAccount(bookie, userAccount, captchaText);
  }
}
