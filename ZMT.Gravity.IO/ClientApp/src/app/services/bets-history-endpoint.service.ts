import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';
import { RealTimeBetView, RealTimeBetResponse } from '../models/realtimebet.model';
import { Observable } from 'rxjs';


@Injectable()
export class BetsHistoryEndpoint extends EndpointFactory {
  private readonly _betsUrl: string = "/api/Bet";

  get betsUrl() {
    return this.configurations.baseUrl + this._betsUrl;
  }
  get exportUrl() {
    return this.configurations.baseUrl + this._betsUrl + "/Export";
  }

  get deltaOddsUrl() { return this.configurations.baseUrl + this._betsUrl + "/DeltaOdds" }

  get lastOddUrl() { return this.configurations.baseUrl + this._betsUrl + "/LastOdd" }

  buildBetDataByUsernameUrl(bookie: string, userName: string) {
    return this.configurations.baseUrl + this._betsUrl + "/" + bookie + "?UserName=" + userName;
  }

  buildBetDataByMatchUrl(bookie: string, matchId: string) {
    return this.configurations.baseUrl + this._betsUrl + "/" + bookie + "/" + matchId;
  }

  constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {
    super(http, configurations, injector);
  }

  getBets<T>(bookie: string, pageIndex: number, pageSize: number, fromDate: Date, toDate: Date) {
    return this.http.get<T>(this.betsUrl + "/" + bookie + "?fromDate=" + fromDate.toISOString() + "&toDate=" + toDate.toISOString() + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize, this.getRequestHeaders())
      .pipe<T>(catchError(error => {
        return this.handleError(error, () => this.getBets(bookie, pageIndex, pageSize, fromDate, toDate));
      }));
  }

  getRealTimeBets<T>(bookie: string): Observable<T> {
    return this.http.get<RealTimeBetResponse[]>(this.betsUrl + "/" + bookie + "/top", this.getRequestHeaders())
      .pipe(catchError(error => {
        return this.handleError(error, () => this.getRealTimeBets(bookie));
      }));
  }

  getDeltaOdds(obj: RealTimeBetView) {
    return this.http.post(this.deltaOddsUrl, obj, this.getRequestHeaders())
      .pipe(catchError(error => {
        console.log(obj.id + " GET DeltaOdds = " + JSON.stringify(error));
        return this.handleError(error, () => this.getDeltaOdds(obj));
      }));
  }

  getLastOdd(obj: RealTimeBetView) {
    return this.http.post(this.lastOddUrl, obj, this.getRequestHeaders())
      .pipe(catchError(error => {
        console.log(obj.id + " GET LastOdd = " + JSON.stringify(error));
        return this.handleError(error, () => this.getLastOdd(obj));
      }));
  }

  getBetsByUserName(bookie: string, userName: string, pageIndex: number, pageSize: number, fromDate: Date, toDate: Date) {
    return this.http.get(this.buildBetDataByUsernameUrl(bookie, userName) + "&fromDate=" + fromDate.toISOString() + "&toDate=" + toDate.toISOString() + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize, this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.getBetsByUserName(bookie, userName, pageIndex, pageSize, fromDate, toDate));
    }));
  }

  getBetsByMatch(bookie: string, matchId: string, pageIndex: number, pageSize: number, fromDate: Date, toDate: Date) {

    return this.http.get(this.buildBetDataByMatchUrl(bookie, matchId) + "?fromDate=" + fromDate.toISOString() + "&toDate=" + toDate.toISOString() + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize, this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.getBetsByMatch(bookie, matchId, pageIndex, pageSize, fromDate, toDate));
    }));
  }

  getExport<Blob>(bookie: string): Observable<Blob> {
    var headers = this.getRequestHeaders();
    headers["responseType"] = 'blob';

    return this.http.get<Blob>(this.exportUrl + "?bookie=" + bookie, headers).pipe(
      catchError(error => {
        return this.handleError(error, () => this.getExport<Blob>(bookie));
      }));
  }
}
