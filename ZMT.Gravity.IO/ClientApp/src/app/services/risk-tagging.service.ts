import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RiskTaggingEndpoint } from './risk-tagging-endpoint.service';
import { RiskTaggingEdit, RiskTaggingResponse } from '../models/risk-tagging.model';
import { RiskTaggingSearch } from '../models/search.model';

@Injectable()
export class RiskTaggingService {

  constructor(private RiskTaggingEndpoint: RiskTaggingEndpoint) { }

  getRiskTaggings(searchParams: RiskTaggingSearch): Observable<any> {
    return this.RiskTaggingEndpoint.getRiskTaggings(searchParams);
  }

  deleteRiskTagging(RiskTagging: RiskTaggingResponse) {
    return this.RiskTaggingEndpoint.deleteRiskTagging(RiskTagging);
  }

  newRiskTagging(RiskTagging: RiskTaggingEdit): Observable<any> {
    return this.RiskTaggingEndpoint.newRiskTagging(RiskTagging);
  }

  get(riskTagging: RiskTaggingResponse) {
    return this.RiskTaggingEndpoint.get(riskTagging);
  }

  editRiskTagging(RiskTagging: RiskTaggingEdit): Observable<any> {
    return this.RiskTaggingEndpoint.editRiskTagging(RiskTagging);
  }

  getExport(searchParams: RiskTaggingSearch) {
    return this.RiskTaggingEndpoint.getExport(searchParams);
  }

  import(formData: FormData) {
    return this.RiskTaggingEndpoint.import(formData);
  }

  getFieldList(): Observable<any> {
    return this.RiskTaggingEndpoint.getFieldList();
  }

  updateFieldList(data) {
    return this.RiskTaggingEndpoint.updateFieldList(data);
  }
}
