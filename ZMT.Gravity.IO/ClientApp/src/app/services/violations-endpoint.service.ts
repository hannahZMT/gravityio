import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';
import { Observable } from 'rxjs';
import { ViolationResponse } from '../models/violation.model';
import { ViolationSearch } from '../models/search.model';
import { formatDate } from '@angular/common';
import { isNullOrUndefined } from 'util';
import { MemberRiskTagResponse } from '../models/member-risk-tag.model';


@Injectable()
export class ViolationsEndpoint extends EndpointFactory {
  private readonly _url: string = "/api/Violations";

  get url() {
    return this.configurations.baseUrl + this._url;
  }

  get detailUrl() {
    return this.configurations.baseUrl + this._url + '/Detail';
  }

  get updatePtUrl() {
    return this.configurations.baseUrl + this._url + '/UpdatePt'
  }

  constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {
    super(http, configurations, injector);
  }

  getAll<T>(searchParams: ViolationSearch): Observable<T> {
    return this.http.get<T>(this.url + searchParams.url, this.getRequestHeaders())
      .pipe<T>(catchError(error => {
        return this.handleError(error, () => this.getAll<T>(searchParams));
      }));
  }

  update<T>(obj: ViolationResponse): Observable<T> {
    let id = obj.id;
    return this.http.put<T>(this.url + '/' + id, obj, this.getRequestHeaders())
      .pipe<T>(catchError(error => {
        return this.handleError(error, () => this.update<T>(obj));
      }));
  }

  updatePT<T>(obj: MemberRiskTagResponse): Observable<T> {
    return this.http.put<T>(this.updatePtUrl, obj, this.getRequestHeaders())
      .pipe<T>(catchError(error => {
        return this.handleError(error, () => this.updatePT<T>(obj));
      }));
  }

  detail<T>(id: string): Observable<T> {
    return this.http.get<T>(this.detailUrl + '/' + id, this.getRequestHeaders())
      .pipe<T>(catchError(error => {
        return this.handleError(error, () => this.detail<T>(id));
      }));
  }
}
