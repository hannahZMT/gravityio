import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';
import { AgentAccountEdit } from '../models/agent-account.model';
import { AgentAccountSearch } from '../models/search.model';
import { Observable } from 'rxjs';
import { AgentAccountStatus } from '../models/enums';

@Injectable()
export class AgentAccountNEndpoint extends EndpointFactory {

  private readonly _agentAccountUrl: string = "/api/AgentAccountMongo";
  private readonly _url: string = this.configurations.baseUrl + this._agentAccountUrl;

  constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {
    super(http, configurations, injector);
  }

  getAll<T>(searchParams: AgentAccountSearch): Observable<T> {
    return this.http.get(this._url + searchParams.Url, this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.getAll(searchParams));
    }));
  }

  getObj(key: string) {
    return this.http.get(this._url + "/" + key, this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.getObj(key));
    }));
  }

  addObj<T>(agentAccount: AgentAccountEdit): Observable<T> {
    return this.http.post(this._url, agentAccount, this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.addObj(agentAccount));
    }));
  }

  delObj<T>(key: string): Observable<T> {
    return this.http.delete(this._url + "/" + key, this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.delObj(key));
    }));
  }

  updObj<T>(agentAccount: AgentAccountEdit): Observable<T> {
    return this.http.put(this._url, agentAccount, this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.updObj(agentAccount));
    }));
  }

  changeAgentStatus(bookie: string, username: string, newStatus: AgentAccountStatus) {
    return this.http.put(this._url + "/" + bookie + "/" + username + "/Status/" + newStatus, '', this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.changeAgentStatus(bookie, username, newStatus));
    }));
  }

  setCaptchaByUserAccount(bookie: string, userAccount: string, captchaText: string) {
    return this.http.put(this._url + "/" + bookie + '/' + userAccount + '/Captcha/' + captchaText, '', this.getRequestHeaders())
      .pipe(catchError(error => {
        return this.handleError(error, () => this.setCaptchaByUserAccount(bookie, userAccount, captchaText));
      }));
  }
}
