import { Injectable } from '@angular/core';
import { SmartRuleEndpoint } from './smart-rule-endpoint.service';
import { SmartRuleResponse, SmartRuleEdit } from '../models/smart-rule.model';

@Injectable()
export class SmartRuleService {
  constructor(private SmartRuleEndPoint: SmartRuleEndpoint) { }

  get(obj: SmartRuleResponse) {
    return this.SmartRuleEndPoint.get(obj);
  }

  getAll() {
    return this.SmartRuleEndPoint.getAll();
  }

  create(obj: SmartRuleEdit) {
    return this.SmartRuleEndPoint.create(obj);
  }

  update(obj: SmartRuleEdit) {
    return this.SmartRuleEndPoint.update(obj);
  }

  delete(id: string) {
    return this.SmartRuleEndPoint.delete(id);
  }

  logs(id: string){
    return this.SmartRuleEndPoint.logs(id);
  }
}
