import { Injectable } from '@angular/core';
import { BetsHistoryEndpoint } from './bets-history-endpoint.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { EndpointFactory } from './endpoint-factory.service';
import { RealTimeBetView } from '../models/realtimebet.model';

@Injectable()
export class BetsHistoryService {
  constructor(private BetsHistoryEndpoint: BetsHistoryEndpoint) {
  }

  getBets(bookie: string, pageIndex: number, pageSize: number, fromDate: Date, toDate: Date) {
    return this.BetsHistoryEndpoint.getBets(bookie, pageIndex, pageSize, fromDate, toDate);
  }

  getDeltaOdds(obj: RealTimeBetView) {
    return this.BetsHistoryEndpoint.getDeltaOdds(obj);
  }

  getLastOdd(obj: RealTimeBetView) {
    return this.BetsHistoryEndpoint.getLastOdd(obj);
  }

  getRealTimeBets(bookie: string) {
    return this.BetsHistoryEndpoint.getRealTimeBets(bookie);
  }

  getBetsbyUserName(bookie: string, userName: string, fromDate: Date, toDate: Date, pageIndex: number, pageSize: number) {
    return this.BetsHistoryEndpoint.getBetsByUserName(bookie, userName, pageIndex, pageSize, fromDate, toDate);
  }

  getBetsbyMatch(bookie: string, matchId: string, pageIndex: number, pageSize: number, fromDate: Date, toDate: Date) {
    return this.BetsHistoryEndpoint.getBetsByMatch(bookie, matchId, pageIndex, pageSize, fromDate, toDate);
  }

  getExport(bookie: string) {
    return this.BetsHistoryEndpoint.getExport(bookie);
  }
}
