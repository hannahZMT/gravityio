import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CountryEndpoint } from './country-endpoint.service';
import { Observable } from 'rxjs';

@Injectable()
export class CountryService {

    constructor(
        private http: HttpClient,
        private countryEndpoint: CountryEndpoint) {
    }

    getCountries(): Observable<any> {
        return this.countryEndpoint.getCountries();
    }
}
