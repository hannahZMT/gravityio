import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';


@Injectable()
export class ConfigurationEndpoint extends EndpointFactory {

  private readonly _configUrl: string = "/api/UserAccount/UserSetting";

  getConfigUrl() { return this.configurations.baseUrl + this._configUrl; }

  constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

    super(http, configurations, injector);
  }

  getConfig<T>(): Observable<T> {
    return this.http.get(this.getConfigUrl(), this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.getConfig());
    }));
  }

  setConfig<T>(json: string): Observable<T> {
    return this.http.post(this.getConfigUrl() + "/" + json, null, this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.setConfig(json));
    }));
  }
}
