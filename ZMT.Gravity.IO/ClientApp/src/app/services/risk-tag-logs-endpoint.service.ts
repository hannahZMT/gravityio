import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';
import { RiskTagLogSearch } from '../models/search.model';
import { Observable } from 'rxjs';


@Injectable()
export class RiskTagLogsEndpoint extends EndpointFactory {

  private readonly _url: string = "/api/RiskTag/logs";


  get getAllUrl() {
    return this.configurations.baseUrl + this._url;
  }

  constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {
    super(http, configurations, injector);
  }

  getAll<T>(searchParams: RiskTagLogSearch): Observable<T> {
    return this.http.get<T>(this.getAllUrl + searchParams.url, this.getRequestHeaders())
      .pipe<T>(catchError(error => {
        return this.handleError(error, () => this.getAll<T>(searchParams));
      }));
  }
}
