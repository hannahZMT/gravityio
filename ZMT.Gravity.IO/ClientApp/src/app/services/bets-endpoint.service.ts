import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';
import { Observable } from 'rxjs';
import { BetsHistorySearch } from '../models/search.model';
import { isUndefined } from 'util';

@Injectable()
export class BetsEndpoint extends EndpointFactory {
  constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {
    super(http, configurations, injector);
  }

  get queryUrl() {
    return this.configurations.baseUrl + "/api/Query/Filter";
  }

  get exportUrl() {
    return this.configurations.baseUrl + "/api/Query/Export";
  }

  getAll(searchParams: BetsHistorySearch) {
    return this.http.get(this.queryUrl + searchParams.url, this.getRequestHeaders()).pipe(
      catchError(error => {
        return this.handleError(error, () => this.getAll(searchParams));
      }));
  }

  export<Blob>(searchParams: BetsHistorySearch, showPnL?: boolean): Observable<Blob> {
    var headers = this.getRequestHeaders();
    headers["responseType"] = 'blob';

    let url = this.exportUrl + searchParams.exportUrl;
    if (showPnL) url += "&showPnL=" + showPnL;
    return this.http.get<Blob>(url, headers).pipe(
      catchError(error => {
        return this.handleError(error, () => this.export<Blob>(searchParams));
      }));
  }
}
