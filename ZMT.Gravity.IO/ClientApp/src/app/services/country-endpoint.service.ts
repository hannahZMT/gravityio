import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';


@Injectable()
export class CountryEndpoint extends EndpointFactory {

  private readonly _countriesUrl: string = "/api/RiskTag/countries";

  getContriesUrl() { return this.configurations.baseUrl + this._countriesUrl; }

  constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

    super(http, configurations, injector);
  }

  getCountries() {
    return this.http.get(this.getContriesUrl(), this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.getCountries());
    }));
  }
}
