import { Injectable } from '@angular/core';
import { BetsEndpoint } from './bets-endpoint.service';
import { BetsHistorySearch } from '../models/search.model';

@Injectable()
export class BetsService {

  constructor(private BetsEndpoint: BetsEndpoint) {
  }

  getAll(searchParams: BetsHistorySearch) {
    return this.BetsEndpoint.getAll(searchParams);
  }

  export(searchParams: BetsHistorySearch, showPnL?: boolean) {
    return this.BetsEndpoint.export(searchParams, showPnL);
  }
}
