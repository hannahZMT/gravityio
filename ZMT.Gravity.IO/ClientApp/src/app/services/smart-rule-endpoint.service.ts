import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';
import { Observable } from 'rxjs';
import { SmartRuleResponse, SmartRuleEdit } from '../models/smart-rule.model';


@Injectable()
export class SmartRuleEndpoint extends EndpointFactory {
  private readonly _smartRuleUrl: string = "/api/SmartRule";

  get smartRuleUrl() {
    return this.configurations.baseUrl + this._smartRuleUrl;
  }

  constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {
    super(http, configurations, injector);
  }

  get<T>(obj: SmartRuleResponse): Observable<T> {
    return this.http.get<T>(this.smartRuleUrl + '/' + obj.ruleId, this.getRequestHeaders())
      .pipe<T>(catchError(error => {
        return this.handleError(error, () => this.get<T>(obj));
      }));
  }

  getAll<T>(): Observable<T> {
    return this.http.get<T>(this.smartRuleUrl, this.getRequestHeaders())
      .pipe<T>(catchError(error => {
        return this.handleError(error, () => this.getAll<T>());
      }));
  }

  create<T>(obj: SmartRuleEdit): Observable<T> {
    return this.http.post<T>(this.smartRuleUrl, obj, this.getRequestHeaders())
      .pipe<T>(catchError(error => {
        return this.handleError(error, () => this.create<T>(obj));
      }));
  }

  update<T>(obj: SmartRuleEdit): Observable<T> {
    let id = obj.ruleId;
    return this.http.put<T>(this.smartRuleUrl + '/' + id, obj, this.getRequestHeaders())
      .pipe<T>(catchError(error => {
        return this.handleError(error, () => this.update<T>(obj));
      }));
  }

  delete<T>(id: string): Observable<T> {
    return this.http.delete<T>(this.smartRuleUrl + '/' + id, this.getRequestHeaders())
      .pipe<T>(catchError(error => {
        return this.handleError(error, () => this.delete<T>(id));
      }));
  }

  logs<T>(id: string): Observable<T> {
    let params = "/logs/" + id;
    return this.http.get<T>(this.smartRuleUrl + params, this.getRequestHeaders())
      .pipe<T>(catchError(error => {
        return this.handleError(error, () => this.logs<T>(id));
      }));
  }
}
