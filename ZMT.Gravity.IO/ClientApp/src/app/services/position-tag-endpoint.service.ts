import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';

import { PositionTag } from '../models/position-tag.model';

@Injectable()
export class PositionTagEndpoint extends EndpointFactory {

  private readonly _riskTaggingUrl: string = "/api/RiskTag/pt";

  getUrl() {
    return this.configurations.baseUrl + this._riskTaggingUrl;
  }

  newUrl() {
    return this.configurations.baseUrl + this._riskTaggingUrl;
  }

  constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {
    super(http, configurations, injector);
  }


  getAll() {
    return this.http.get(this.getUrl(), this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.getAll());
    }));
  }

  newObject(obj: PositionTag): Observable<any> {
    return this.http.post(this.newUrl(), obj, this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.newObject(obj));
    }));
  }
}
