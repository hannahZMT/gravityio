import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpParams, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';

import { RiskTaggingEdit, RiskTaggingResponse } from '../models/risk-tagging.model';
import { RiskTaggingSearch } from '../models/search.model';

@Injectable()
export class RiskTaggingEndpoint extends EndpointFactory {

  private readonly _riskTaggingUrl: string = "/api/RiskTag";

  get riskTaggingUrl() {
    return this.configurations.baseUrl + this._riskTaggingUrl;
  }

  editRiskTaggingUrl(id: string) {
    return this.configurations.baseUrl + this._riskTaggingUrl + "/" + id;
  }

  deleteRiskTaggingUrl(id: string) {
    return this.configurations.baseUrl + this._riskTaggingUrl + "/" + id;
  }

  get newRiskTaggingUrl() {
    return this.configurations.baseUrl + this._riskTaggingUrl;
  }

  get exportUrl() {
    return this.configurations.baseUrl + this._riskTaggingUrl + "/Export";
  }

  get importUrl() {
    return this.configurations.baseUrl + this._riskTaggingUrl + "/upload";
  }

  get fieldListUrl() {
    return this.configurations.baseUrl + this._riskTaggingUrl + "/Setting";
  }

  constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {
    super(http, configurations, injector);
  }

  getRiskTaggings(searchParams: RiskTaggingSearch) {
    return this.http.get(this.riskTaggingUrl + searchParams.url, this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.getRiskTaggings(searchParams));
    }));
  }

  get(riskTagging: RiskTaggingResponse) {
    return this.http.get(this.editRiskTaggingUrl(riskTagging.id), this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.get(riskTagging));
    }));
  }

  editRiskTagging(riskTagging: RiskTaggingEdit) {
    return this.http.put(this.editRiskTaggingUrl(riskTagging.id), riskTagging, this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.editRiskTagging(riskTagging));
    }));
  }

  deleteRiskTagging(riskTagging: RiskTaggingResponse) {
    return this.http.delete(this.deleteRiskTaggingUrl(riskTagging.id), this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.deleteRiskTagging(riskTagging));
    }));
  }

  newRiskTagging(riskTagging: RiskTaggingEdit): Observable<any> {
    return this.http.post(this.newRiskTaggingUrl, riskTagging, this.getRequestHeaders()).pipe(catchError(error => {
      return this.handleError(error, () => this.newRiskTagging(riskTagging));
    }));
  }

  getExport<Blob>(searchParams: RiskTaggingSearch): Observable<Blob> {
    var headers = this.getRequestHeaders();
    headers["responseType"] = 'blob';

    return this.http.get<Blob>(this.exportUrl + searchParams.exportUrl, headers).pipe(
      catchError(error => {
        return this.handleError(error, () => this.getExport<Blob>(searchParams));
      }));
  }

  import(formData: FormData): Observable<any> {
    const uploadReq = new HttpRequest('POST', this.importUrl, formData, this.getRequestHeadersToUploadFile());
    return this.http.request(uploadReq)
      .pipe(catchError(error => {
        return this.handleError(error, () => this.import(formData));
      }));
  }

  getFieldList() {
    return this.http.get(this.fieldListUrl, this.getRequestHeaders())
      .pipe(catchError(error => {
        return this.handleError(error, () => this.getFieldList());
      }));
  }

  updateFieldList(data) {
    return this.http.post(this.fieldListUrl, data, this.getRequestHeaders())
      .pipe(catchError(error => {
        return this.handleError(error, () => this.updateFieldList(data));
      }));;
  }
}
