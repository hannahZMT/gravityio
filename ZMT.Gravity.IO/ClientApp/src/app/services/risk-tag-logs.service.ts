import { Injectable } from '@angular/core';
import { RiskTagLogsEndpoint } from './risk-tag-logs-endpoint.service';
import { RiskTagLogSearch } from '../models/search.model';

@Injectable()
export class RiskTagLogsService {

  constructor(private Endpoint: RiskTagLogsEndpoint) { }

  getAll(searchParams: RiskTagLogSearch) {
    return this.Endpoint.getAll(searchParams);
  }
}
