import { Injectable } from '@angular/core';
import { ViolationsEndpoint } from './violations-endpoint.service';
import { ViolationResponse } from '../models/violation.model';
import { ViolationSearch } from '../models/search.model';
import { MemberRiskTagResponse } from '../models/member-risk-tag.model';

@Injectable()
export class ViolationsService {
  constructor(private endPoint: ViolationsEndpoint) { }

  getAll(searchParams: ViolationSearch) {
    return this.endPoint.getAll(searchParams);
  }

  update(obj: ViolationResponse) {
    return this.endPoint.update(obj);
  }

  updatePT(obj: MemberRiskTagResponse) {
    return this.endPoint.updatePT(obj);
  }

  detail(id: string) {
    return this.endPoint.detail(id);
  }
}
