import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PositionTag } from '../models/position-tag.model';
import { PositionTagEndpoint } from './position-tag-endpoint.service';

@Injectable()
export class PositionTagService {

  constructor(private PositionTagEndpoint: PositionTagEndpoint) { }

  getAll(): Observable<any> {
    return this.PositionTagEndpoint.getAll();
  }

  newObject(PositionTag: PositionTag): Observable<any> {
    return this.PositionTagEndpoint.newObject(PositionTag);
  }
}
